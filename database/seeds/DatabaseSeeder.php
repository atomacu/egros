<?php

use App\Role;
use App\Langs;
use App\UserRole;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roleId = Role::store('Admin',"Poate face tot ce vrea.",1,'admin',0,0);
        Role::store('Moderator',"Poate face tot ce vrea.",1,'Moderator',0,0);
        $roleId1 = Role::store('Shop admin',"Nu poate face chiar tot ce vrea.",1,'shop_admin',1,1);
        Role::store('Shop moderator',"Nu poate face tot ce vrea.",0,'shop_moderator',0,1);

        $userId = DB::table('users')->insertGetId([
            'name' => "Tomacu Alexandru",
            'email' => "atomacu@gmail.com",
            'password' => Hash::make('1234567890'),
        ]);

        $userId1 = DB::table('users')->insertGetId([
            'name' => "Tomacu Alexandru",
            'email' => "moderator@gmail.com",
            'password' => Hash::make('1234567890'),
        ]);

        UserRole::store($userId,$roleId);

        UserRole::store($userId1,$roleId1);

        Langs::create([
            'name' => "Ro",
            'system_name' => 'ro'
        ]);
        Langs::create([
            'name' => "Ru",
            'system_name' => 'ru'
        ]);
    }
}
