<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_props', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prod_id')->unsigned();
            $table->foreign('prod_id')->references('id')->on('products')->onDelete('cascade');
            $table->bigInteger('prod_prop_id')->unsigned();
            $table->foreign('prod_prop_id')->references('id')->on('prorpieties')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_props');
    }
}
