<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPropValsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prop_vals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prod_prop_val_id')->unsigned();
            $table->foreign('prod_prop_val_id')->references('id')->on('product_props')->onDelete('cascade');
            $table->string('value');
            $table->string('priceDiff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prop_vals');
    }
}
