<?php

namespace App;

use App\Langs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prorpiety extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'type_id','name','val_is_number'
    ];

    public function type(){
        return $this->belongsTo('App\Type','type_id');
    }

    public function getProprietyTrans($langSysName){
        $lang=Langs::where('system_name',$langSysName)->first();
        if($lang){
            return $this->hasMany('App\ProrpietyTrans',"prop_id")->where('lang_id',$lang->id)->first();
        }
    }

    public function propVal(){
        return $this->hasMany('App\PropVal',"prop_id");
    }

    public function propProducts(){
        return $this->hasMany('App\ProductPropVal',"prod_prop_val_id");
    }
}
