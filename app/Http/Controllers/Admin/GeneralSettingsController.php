<?php

namespace App\Http\Controllers\Admin;

use App\Langs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $langs=Langs::all();
        return view('admin.general-settings.index',compact('langs'));
    }
}
