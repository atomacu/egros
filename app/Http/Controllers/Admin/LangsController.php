<?php

namespace App\Http\Controllers\Admin;

use App\Langs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LangsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $i=count(Langs::all());
        $systemName=strtolower($request['name']);
        $lang=Langs::create([
            'name'=>$request['name'],
            'system_name'=>$systemName
        ]);
        $html = view('admin.general-settings.components.langs.new-lang-row', compact('lang','i'))->render();
        return json_encode($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $systemName=strtolower($request['name']);
        Langs::where("id",$id)->update([
            "name"=>$request['name'],
            'system_name'=>$systemName
        ]);
        $lang=Langs::find($id);
        return json_encode($lang);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lang=Langs::find($id);
        $lang->delete();
        return $id;
    }
}
