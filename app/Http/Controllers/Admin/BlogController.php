<?php

namespace App\Http\Controllers\Admin;

use File;
use Auth;
use App\Langs;
use App\BlogPosts;
use App\BlogPostsTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $posts=BlogPosts::all();
        $langs = Langs::all();
        return view('admin.blog.index',compact('langs','posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id=BlogPosts::create([
            'user_id' => Auth::user()->id
        ])->id;


        
        $image = $request['img'];  
        $virgula=strrpos($image,',');
        $str=substr($image, 0,$virgula+1);
        $bara=strrpos($image,'image/');
        $punct=strrpos($image,';base64');
        $extension= substr($str, $bara+6,$punct-$bara-6);
        $image = str_replace('data:image/'.$extension.';base64,', '', $image);
        $image = str_replace(' ', '+', $image);

        $imageName = 'post-cover-img-'.$id.'.'.$extension;
        
        File::put('images/cover/'. $imageName, base64_decode($image));

        BlogPosts::where('id',$id)->update([
            "img" => $imageName
        ]);
        
        foreach(json_decode($request['postTrans'],true) as $postTitle){
            BlogPostsTrans::create([
                'title' => $postTitle['title'],
                'description' => $postTitle['description'],
                'post_id' => $id,
                'lang_id' => $postTitle['langId']
            ]);

        }
        // /Applications/XAMPP/xamppfiles/htdocs/egros/resources/views/admin/general-settings/components/blog/new-post-form.blade.php
      
        return $id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = BlogPosts::find($id);
        return view('admin.blog.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = BlogPosts::find($id);
        return view('admin.blog.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request['imgChanged']==1){
            $image = $request['img'];  
            $virgula=strrpos($image,',');
            $str=substr($image, 0,$virgula+1);
            $bara=strrpos($image,'image/');
            $punct=strrpos($image,';base64');
            $extension= substr($str, $bara+6,$punct-$bara-6);
            $image = str_replace('data:image/'.$extension.';base64,', '', $image);
            $image = str_replace(' ', '+', $image);

            $imageName = 'post-cover-img-'.$id.'.'.$extension;
            
            File::put('images/cover/'. $imageName, base64_decode($image));

            BlogPosts::where('id',$id)->update([
                "img" => $imageName
            ]);
        }
        
        foreach(json_decode($request['postTrans'],true) as $postTitle){
            BlogPostsTrans::where('id',$postTitle['postId'])->update([
                'title' => $postTitle['title'],
                'description' => $postTitle['description'],
            ]);
        }
     
        return route('admin-blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BlogPosts::where('id',$id)->delete();
        return $id;
    }
}
