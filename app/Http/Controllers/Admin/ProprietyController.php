<?php

namespace App\Http\Controllers\Admin;

use App\Langs;
use App\Prorpiety;
use App\ProrpietyTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProprietyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $i = count(Prorpiety::all());
        $propriety = Prorpiety::create([
            "type_id" => $request['typeId'],
            "name" => $request['name'],
            "val_is_number" => $request['valIsNumber']
        ]);
        foreach($request["namesTrans"] as $name){
            ProrpietyTrans::create([
                "name" => $name['name'],
                'lang_id' => $name['id'],
                'prop_id' => $propriety->id
            ]);
        }
        $langs = Langs::all();
        $html = view('admin.general-settings.components.categories.types.proprieties.propriety-card',compact('langs','propriety'))
        ->render();
        return json_encode($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $langs=Langs::all();
        $propriety=Prorpiety::find($id);
        return view('admin.general-settings.categories.types.proprieties.show',compact('propriety','langs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        Prorpiety::where('id',$id)->update([
            "name" => $request['name'],
            "val_is_number" => $request['valIsNumber']
        ]);
        foreach($request["namesTrans"] as $name){
            ProrpietyTrans::where('id',$name['id'])->update([
                "name" => $name['name']
            ]);
        }

       
        $propriety=Prorpiety::find($id);
        $html = view('admin.general-settings.components.categories.types.propriety-card',compact('langs','propriety'))->render();
        return json_encode($html);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Prorpiety::where('id',$id)->delete();
        return 1;
    }
}
