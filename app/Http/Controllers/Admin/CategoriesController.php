<?php

namespace App\Http\Controllers\Admin;

use File;
use App\Type;
use App\Langs;
use App\Category;
use App\Prorpiety;
use App\CategoryTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $langs=Langs::all();
        $categories=Category::all();
        $html = view('admin.general-settings.components.categories.categories-table',compact('categories','langs'))->render();
        return json_encode($html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs=Langs::all();
        $html = view('admin.general-settings.components.categories.categories-modal-add-form',compact('langs'))->render();
        return json_encode($html);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $i=count(Category::all());
        $category=Category::create([
            "name"=>$request['generalName']
        ]);
        foreach($request["names"] as $name){
            CategoryTrans::create([
                "name"=>$name['name'],
                'lang_id'=>$name['id'],
                'category_id'=>$category->id
            ]);
        }

    
        $image = $request['img'];  
        $virgula=strrpos($image,',');
        $str=substr($image, 0,$virgula+1);
        $bara=strrpos($image,'image/');
        $punct=strrpos($image,';base64');
        $extension= substr($str, $bara+6,$punct-$bara-6);
        $image = str_replace('data:image/'.$extension.';base64,', '', $image);
        $image = str_replace(' ', '+', $image);

        $imageName = 'category-image-'.$category->id.'.'.$extension;
 
        File::put('images/categories/'. $imageName, base64_decode($image));

        Category::where('id',$category->id)->update([
            'img' => $imageName
        ]);
        $category=Category::find($category->id);
        $langs=Langs::all();
        $html = view('admin.general-settings.components.categories.category-table-row',compact('langs','category','i'))->render();
        return json_encode($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $langs=Langs::all();
        $category=Category::find($id);
        $types=Type::all();
        return view('admin.general-settings.categories.show',compact('langs','category','types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Category::where('id',$id)->update([
            "name"=>$request['name']
        ]);
        foreach($request["nameTrans"] as $nameTrans){
            $category=CategoryTrans::where('category_id',$id)->where('lang_id',$nameTrans['id'])->first();

            if($category){
                CategoryTrans::where('category_id',$id)->where('lang_id',$nameTrans['id'])->update([
                    "name"=>$nameTrans['name'],
                ]);
            }else{
                CategoryTrans::create([
                    "name"=>$nameTrans['name'],
                    'lang_id'=>$nameTrans['id'],
                    'category_id'=>$id
                ]);
            }
        }

        if($request['img']!=""){
            $image = $request['img'];  
            $virgula=strrpos($image,',');
            $str=substr($image, 0,$virgula+1);
            $bara=strrpos($image,'image/');
            $punct=strrpos($image,';base64');
            $extension= substr($str, $bara+6,$punct-$bara-6);
            $image = str_replace('data:image/'.$extension.';base64,', '', $image);
            $image = str_replace(' ', '+', $image);
    
            $imageName = 'category-image-'.$id.'.'.$extension;
     
            File::put('images/categories/'. $imageName, base64_decode($image));
    
            Category::where('id',$id)->update([
                'img' => $imageName
            ]);
        }
        

        $langs=Langs::all();
        $categories=Category::all();
        $html = view('admin.general-settings.components.categories.categories-table',compact('categories','langs'))->render();
        return json_encode($html);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Category::where('id',$id)->delete();
       return 1;
    }
}
