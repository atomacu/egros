<?php

namespace App\Http\Controllers\Admin;

use File;
use App\Type;
use App\Langs;
use App\TypeTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $i = count(Type::all());
        $type = Type::create([
            "category_id" => $request['catId'],
            "name" => $request['name']
        ]);
        foreach($request["namesTrans"] as $name){
            TypeTrans::create([
                "name" => $name['name'],
                'lang_id' => $name['id'],
                'type_id' => $type->id
            ]);
        }

        $image = $request['img'];  
        $virgula=strrpos($image,',');
        $str=substr($image, 0,$virgula+1);
        $bara=strrpos($image,'image/');
        $punct=strrpos($image,';base64');
        $extension= substr($str, $bara+6,$punct-$bara-6);
        $image = str_replace('data:image/'.$extension.';base64,', '', $image);
        $image = str_replace(' ', '+', $image);

        $imageName = 'type-image-'.$type->id.'.'.$extension;
 
        File::put('images/types/'. $imageName, base64_decode($image));

        Type::where('id',$type->id)->update([
            'img' => $imageName
        ]);

        $langs = Langs::all();
        $html = view('admin.general-settings.components.categories.types.type-card',compact('langs','type','i'))->render();
        return json_encode($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Type::where('id',$id)->update([
            "name"=>$request['name']
        ]);
        foreach($request["namesTrans"] as $nameTrans){
            TypeTrans::where('type_id',$id)->where('lang_id',$nameTrans['id'])->update([
                "name"=>$nameTrans['name'],
            ]);
        }

        if($request['img']!=""){
            $image = $request['img'];  
            $virgula=strrpos($image,',');
            $str=substr($image, 0,$virgula+1);
            $bara=strrpos($image,'image/');
            $punct=strrpos($image,';base64');
            $extension= substr($str, $bara+6,$punct-$bara-6);
            $image = str_replace('data:image/'.$extension.';base64,', '', $image);
            $image = str_replace(' ', '+', $image);
    
            $imageName = 'type-image-'.$id.'.'.$extension;
     
            File::put('images/types/'. $imageName, base64_decode($image));
    
            Type::where('id',$id)->update([
                'img' => $imageName
            ]);
        }
        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Type::where('id',$id)->delete();
        return 1;
    }
}
