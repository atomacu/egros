<?php

namespace App\Http\Controllers\Admin;

use App\Images;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImagesUploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request)
    {
        $id=Images::create()->id;
        $image = $request->file('upload');
        $new_name = 'post-img-'.$id.'.'. $image->getClientOriginalExtension();
        $image->move('./posts/images', $new_name);
        Images::setImg($id,$new_name);
       
        $img = array('uploaded' => true, 'url' => '/images/posts/'.$new_name);
        $img = json_encode($img);
        return $img;
    }
}
