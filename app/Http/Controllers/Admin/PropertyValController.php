<?php

namespace App\Http\Controllers\Admin;

use App\Langs;
use App\PropVal;
use App\PropValTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyValController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $i = count(PropVal::all());
        $propVal = PropVal::create([
            "prop_id" => $request['id'],
            "name" => $request['name']
        ]);
        foreach($request["namesTrans"] as $name){
            PropValTrans::create([
                "name" => $name['name'],
                'lang_id' => $name['id'],
                'prop_val_id' => $propVal->id
            ]);
        }
        $langs = Langs::all();
        $html = view('admin.general-settings.components.categories.types.proprieties.prop-val-tr',compact('langs','propVal'))->render();
        return json_encode($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PropVal::where('id',$id)->update([
            "name" => $request['name']
        ]);
        foreach($request["namesTrans"] as $name){
            PropValTrans::where('id',$name['id'])->update([
                "name" => $name['name']
            ]);
        }
        $propVal=PropVal::find($id);
        return $propVal;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PropVal::where('id',$id)->delete();
        return 1;
    }
}
