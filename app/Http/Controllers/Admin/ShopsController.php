<?php

namespace App\Http\Controllers\Admin;

use File;
use Auth;
use App\Shop;
use App\Role;
use App\ShopMemebers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $shops = Shop::all();
        return view('admin.shop-management.index', compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Shop::create([
            "user_id" => Auth::user()->id,
            "name" => $request['name'],
            "email" => $request['email'],
            "phone" =>  $request['phone'],
            "description" => $request['description']
        ])->id;

        $image = $request['img'];  
        $virgula=strrpos($image,',');
        $str=substr($image, 0,$virgula+1);
        $bara=strrpos($image,'image/');
        $punct=strrpos($image,';base64');
        $extension= substr($str, $bara+6,$punct-$bara-6);
        $image = str_replace('data:image/'.$extension.';base64,', '', $image);
        $image = str_replace(' ', '+', $image);

        $imageName = 'shop-image-'.$id.'.'.$extension;
 
        File::put('images/shops/'. $imageName, base64_decode($image));
        Shop::where('id',$id)->update([
            "img" => $imageName
        ]);
        return $id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles=Role::where('shop_role',1)->get();
        $shop=Shop::find($id);
        return view('admin.shop-management.show',compact('shop','roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shop = Shop::find($id);
        return view('admin.shop-management.edit', compact('shop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Shop::where('id',$id)->update([
            "name" => $request['name'],
            "email" => $request['email'],
            "phone" =>  $request['phone'],
            "description" => $request['description']
        ]);

        if($request['changed']==1){
            $image = $request['img'];  
            $virgula=strrpos($image,',');
            $str=substr($image, 0,$virgula+1);
            $bara=strrpos($image,'image/');
            $punct=strrpos($image,';base64');
            $extension= substr($str, $bara+6,$punct-$bara-6);
            $image = str_replace('data:image/'.$extension.';base64,', '', $image);
            $image = str_replace(' ', '+', $image);
    
            $imageName = 'shop-image-'.$id.'.'.$extension;
     
            File::put('images/shops/'. $imageName, base64_decode($image));
        }
       
        return route('admin-shop.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Shop::where('id',$id)->delete();
        return $id;
    }
}
