<?php

namespace App\Http\Controllers;

use App;
use App\Type;
use App\Shop;
use App\Product;
use App\Category;
use App\BlogPosts;
use App\ProductProp;
use App\ProductPropVal;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function wellcome($langSysName="")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $categories=Category::all();
        if(count($categories)>=6){
            $categories=Category::take(6)->get();
        }

        $shops = Shop::all();
        if(count($shops)>=4){
            $shops=Shop::take(4)->get();
        }

        $posts = BlogPosts::all();
        if(count($posts)>=2){
            $posts=BlogPosts::take(2)->get();
        }

        return view('welcome',compact('langSysName','categories','shops','posts'));
    }

    public function about($langSysName="")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        return view('about',compact('langSysName'));
    }

    public function services($langSysName="")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        return view('services',compact('langSysName'));
    }

    public function contact($langSysName="")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        return view('contact',compact('langSysName'));
    }

    public function categories($langSysName="")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $categories = Category::paginate(9);
        return view('categories',compact('langSysName','categories'));
    }

    public function category($id,$langSysName="")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $types = Type::where("category_id",$id)->paginate(9);
        $category = Category::find($id);
        return view('category',compact('langSysName','types','category'));
    }

    public function shop($id,$langSysName=""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $shop = Shop::find($id);
        $products = Product::where('shop_id',$id)->paginate(6);
        // dump($products[0]);
        return view('shop',compact('langSysName','shop','products'));
    }

    public function type($id,$langSysName=""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $type = Type::find($id);
        $products = Product::where('type_id',$id)->paginate(9);
        return view('type',compact('langSysName','type','products'));
    }

    public function shops($langSysName=""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $shops = Shop::paginate(9);
        return view('shops',compact('langSysName','shops'));
    }

    public function posts($langSysName=""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $posts = BlogPosts::paginate(5);
        return view('posts',compact('langSysName','posts'));
    }

    public function post($id,$langSysName=""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $post = BlogPosts::find($id);
        return view('post',compact('langSysName','post'));
    }

    public function product($id,$langSysName=""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        $products=Product::all();
        if(count($products)>=5){
            $products=Product::take(5)->get();
        }
        App::setLocale($langSysName);
        $product = Product::find($id);
        return view('product',compact('langSysName','product','products'));
    }

    public function wishlist($langSysName=""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        return view('wishlist',compact('langSysName'));
    }

    public function wishlistProduct(Request $request, $langSysName=""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        App::setLocale($langSysName);
        $product = Product::find($request['prodId']);
        $html = view('components.wishlist-product',compact('product','langSysName'))->render();
        return json_encode($html);
    }

    public function getFilterResult(Request $request,$langSysName){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
       
        App::setLocale($langSysName);
        $products=[];

        if(!empty($request['property'])){
            $prodProps = ProductProp::whereIn('prod_prop_id',$request['property'])->get();
            foreach($prodProps as $prodProp){
                array_push($products,$prodProp->product);
            }
        }

        if(!empty($request['propVal'])){
            foreach($products as $product){
                foreach($product->productProp as $productProp){
                    foreach($productProp->prodPropValues as $prodPropValue){
                        foreach($request['propVal'] as $propVal){
                            if($propVal==$prodPropValue->id){
                                array_push($products,$productPropVal->prodProp->product);
                            }
                        }
                    }
                }
               
            }
            // array_splice($array, 1, 1);
          
        }

        dump($products);
        return 1;
    }
}
