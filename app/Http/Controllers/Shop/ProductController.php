<?php

namespace App\Http\Controllers\Shop;

use Auth;
use App\Type;
use App\Langs;
use App\Product;
use App\Category;
use App\ProductDescription;
use App\ProductImages;
use App\ProductProp;
use App\ProductPropVal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = Type::find($request['typeId']);

        $product=Product::create([
            'shop_id' => $request['shopId'],
            'category_id' => $type->category->id,
            'type_id' => $request['typeId'],
            'user_id' => Auth::user()->id,
            'price' => $request['basePrice'],
        ]);

        $prodNameDescs = json_decode($request['prodNameDescs'],true);
        foreach($prodNameDescs as $prodDescript){
            ProductDescription::create([
                "prod_id" => $product->id,
                "lang_id" => $prodDescript['langId'],
                "name" => $prodDescript['name'],
                "description" => $prodDescript['description'],
            ]);
        }

        $validation = Validator::make($request->all(), [
            'images' => 'required'
        ]);
        if(!$validation->fails()){
            $images = $request->file('images');
            foreach($images as $i=>$image){
                $prodImg=ProductImages::create([
                    "prod_id" => $product->id
                ]);
                
                $newName = 'product-'.$product->id.'-'.$prodImg->id.'.'.$image->getClientOriginalExtension();
                $image->move('./images/products', $newName);
                $prodImg->img=$newName;
                $prodImg->save();
            }
        }

        $propValues = json_decode($request['propValues'],true);
        

        foreach($propValues as $prop){
            $propProd=ProductProp::create([
                "prod_prop_id" => $prop['propId'],
                "prod_id" => $product->id,
            ]);

            foreach($prop['propVal'] as $propVal){
                ProductPropVal::create([
                    "prod_prop_val_id" => $propProd->id,
                    "value" => $propVal['value'],
                    "priceDiff" => $propVal['priceDiff'],
                ]);
            }
        }

        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Category::all();
        $product = Product::find($id);
        return view('shop-admin.categories.products.show',compact('product','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::where('id',$id)->delete();
        return $id;
    }
}
