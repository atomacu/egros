<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeTrans extends Model
{
    protected $fillable = [
        'name','type_id','lang_id',
    ];
}
