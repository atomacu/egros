<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropValTrans extends Model
{
    protected $fillable = [
        'name','prop_val_id','lang_id',
    ];
}
