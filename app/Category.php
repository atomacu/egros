<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',"img"
    ];

    public function getCategoryTrans($langSysName){
       
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        $lang=Langs::where('system_name',$langSysName)->first();
        if($lang){
            return $this->hasMany('App\CategoryTrans',"category_id")->where('lang_id',$lang->id)->first();
        }
    }

    public function types(){
        return $this->hasMany('App\Type',"category_id");
    }
}
