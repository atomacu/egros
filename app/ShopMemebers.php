<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopMemebers extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name','email','phone','role_id','shop_id'
    ];
    
    public function role(){
        return $this->belongsTo('App\Role','role_id');
    }

    public function shop(){
        return $this->belongsTo('App\Shop','shop_id');
    }
}
