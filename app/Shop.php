<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name','email','phone','description','img','user_id'
    ];

    public function members(){
        return $this->hasMany('App\ShopMemebers',"shop_id");
    }
    
    public function shopProducts(){
        return $this->hasMany('App\Product',"shop_id");
    }

    public function products($catId){
        return $this->hasMany('App\Product',"shop_id")->where('category_id',$catId)->get();
    }

    
}
