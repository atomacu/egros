<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProrpietyTrans extends Model
{
    protected $fillable = [
        'name','prop_id','lang_id',
    ];
}
