<?php

namespace App;

use App\Langs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogPosts extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'img','user_id'
    ];

    public function postTrans($sysName) {
        $lang=Langs::where('system_name',$sysName)->first();
        if($lang){
            return $this->hasMany('App\BlogPostsTrans', 'post_id')->where('lang_id',$lang->id)->first();
        }
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
