<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropVal extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'prop_id','name'
    ];

    public function propriety(){
        return $this->belongsTo('App\Prorpiety','prop_id');
    }
    
    public function getProprietyValTrans($langId){

        return $this->hasMany('App\PropValTrans',"prop_val_id")->where('lang_id',$langId)->first();
    }
}
