<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Images extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'img'
    ];

    static function setImg($id,$new_name){
        parent::where('id',$id)->update([
            'img'=>$new_name
        ]);
        return 1;
    }
}
