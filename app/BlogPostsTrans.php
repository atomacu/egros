<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPostsTrans extends Model
{
    protected $fillable = [
        'title','description','post_id','lang_id',
    ];
}
