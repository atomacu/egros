<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductProp extends Model
{
    protected $fillable = [
        'prod_id','prod_prop_id',
    ];

    public function product(){
        return $this->belongsTo('App\Product','prod_id');
    }

    public function propriety(){
        return $this->belongsTo('App\Prorpiety','prod_prop_id');
    }
    
    public function prodPropValues(){
        return $this->hasMany('App\ProductPropVal',"prod_prop_val_id");
    }
}
