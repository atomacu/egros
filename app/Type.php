<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'category_id','name','img'
    ];

    public function getTypeTrans($langSysName){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }
        $lang=Langs::where('system_name',$langSysName)->first();
        if($lang){
            return $this->hasMany('App\TypeTrans',"type_id")->where('lang_id',$lang->id)->first();
        }

       
    }

    public function proprieties(){
        return $this->hasMany('App\Prorpiety',"type_id");
    }

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }
}
