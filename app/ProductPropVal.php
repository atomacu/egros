<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPropVal extends Model
{
    protected $fillable = [
        'prod_prop_val_id','value','priceDiff',
    ];

    public function propVal(){
        return $this->belongsTo('App\PropVal',"value");
    }

    public function prodProp(){
        return $this->belongsTo('App\ProductProp','prod_prop_val_id');
    }
}
