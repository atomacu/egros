<?php

namespace App;

use App\Langs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'shop_id','category_id','type_id','user_id','price',
    ];
    
    public function type(){
        return $this->belongsTo('App\Type','type_id');
    }

    public function images(){
        return $this->hasMany('App\ProductImages',"prod_id");
    }

    public function prodTrans($sysName) {
        if($sysName == ""){
            $sysName = config('app.fallback_locale'); 
        }
        $lang=Langs::where('system_name',$sysName)->first();
        if($lang){
            return $this->hasMany('App\ProductDescription', 'prod_id')->where('lang_id',$lang->id)->first();
        }
    }

    public function productProp(){
        return $this->hasMany('App\ProductProp',"prod_id");
    }

}
