<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    // use SoftDeletes;
    protected $fillable = [
        'name','description','default_access','default_page','set_default','shop_role'
    ];

    static function store($name,$description,$defaultAccess=0,$defaultPage,$setDefault=0,$shopMember=0){
        return parent::create([
            'name' => $name,
            'description' => $description,
            'default_access' => $defaultAccess,
            'default_page' => $defaultPage,
            'set_default' => $setDefault,
            'shop_role' => $shopMember
        ])->id;
    }

    //  setDefault seteaza daca acest rol 
    //  va fi default atunci cind userul se va inregistra 
    
    static function getDefaultRole(){
        return parent::where('set_default',1)->first();
    }
}
