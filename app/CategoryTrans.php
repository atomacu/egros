<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTrans extends Model
{

    protected $fillable = [
        'name','category_id','lang_id',
    ];
}
