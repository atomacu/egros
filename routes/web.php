<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['role','share']], function(){
    
    Auth::routes(); 
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('admin-lang', 'Admin\LanguagesController');
    Route::resource('admin', 'Admin\AdminController');
    Route::resource('admin-langs', 'Admin\LangsController');
    Route::resource('admin-types', 'Admin\TypesController');
    Route::resource('admin-category', 'Admin\CategoriesController');
    Route::resource('admin-propriety', 'Admin\ProprietyController');
    Route::resource('admin-propriety-val', 'Admin\PropertyValController');
    Route::resource('admin-general-settings', 'Admin\GeneralSettingsController');
    Route::resource('admin-blog', 'Admin\BlogController');
    Route::resource('admin-shop', 'Admin\ShopsController');
    Route::resource('admin-shop-members', 'Admin\ShopMembersController');
    Route::resource('image/upload', 'Admin\ImagesUploadController');
    Route::resource('shop_admin', 'Shop\ShopAdminController');
    Route::resource('shop_admin_category', 'Shop\CategoryController');
    Route::resource('shop_admin_type', 'Shop\TypesController');
    Route::resource('shop_admin_property', 'Shop\PropertyController');
    Route::resource('shop_admin_property_val', 'Shop\PropertyValController');
    Route::resource('shop_admin_product', 'Shop\ProductController');

    
    
    Route::get('/getFilterResult/{lang?}', 'PagesController@getFilterResult')->name('getFilterResult');
    Route::get('/wishlistProduct/{lang?}', 'PagesController@wishlistProduct')->name('wishlistProduct');
    Route::get('/wishlist/{lang?}', 'PagesController@wishlist')->name('wishlist');
    Route::get('/product/{id}/{lang?}', 'PagesController@product')->name('product');
    Route::get('/post/{id}/{lang?}', 'PagesController@post')->name('post');
    Route::get('/posts/{lang?}', 'PagesController@posts')->name('posts');
    Route::get('/shops/{lang?}', 'PagesController@shops')->name('shops');
    Route::get('/shop/{id}/{lang?}', 'PagesController@shop')->name('shop');
    Route::get('/type/{id}/{lang?}', 'PagesController@type')->name('type');
    Route::get('/category/{id}/{lang?}', 'PagesController@category')->name('category');
    Route::get('/categories/{lang?}', 'PagesController@categories')->name('categories');
    Route::get('/contact/{lang?}', 'PagesController@contact')->name('contact');
    Route::get('/services/{lang?}', 'PagesController@services')->name('services');
    Route::get('/about/{lang?}', 'PagesController@about')->name('about');
    Route::get('/{lang?}', 'PagesController@wellcome')->name('wellcome');
});


