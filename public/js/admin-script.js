$(document).ready(function () {

    var imagesPreview = function (input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function (event) {
                    $($.parseHTML('<img class="m-1" style="border:2px solid black; width:23.5%;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('[aria-label="Close"]').click(function () {
        $('.alert-msg').remove();
    });

    $('#add-lang-form').submit(function (e) {
        e.preventDefault();
        var url = $(this).data('url');
        var name = $('#add-lang-name').val();
        var formData = new FormData(this);

        formData.append('name', name);
        $('.alert-msg').remove();
        if (name == "") {
            $('#add-lang-name').parent().append('<p class="alert-msg">Name field is empty.</p>');
        } else {
            $.ajax({
                url: url,
                method: "POST",
                data: formData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#add-lang-name').val("");
                    $('.close').trigger('click');
                    $("#add-lang-modal-tbody").append(data);
                }
            });
        }
    });

    $(document).on('click', '.delete-lang-btn', function (e) {
        let id = $(this).data('id');
        let url = $(this).data('url');

        $.ajax({
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
            },
            dataType: "text",
            success: function (data) {

                $('.close').trigger('click');
                setTimeout(function () {
                    $('#lang-table-row-' + id).remove();
                }, 500);
            },
            error: function (data) {},
        });
    });

    $(document).on('submit', '.edit-lang-form', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        var url = $(this).data('url');
        let name = $('#edit-lang-name-' + id).val();

        $('.alert-msg').remove();
        if (name == "") {
            $('#edit-lang-name-' + id).parent().append('<p class="alert-msg">Name field is empty.</p>');
        } else {
            let json = {
                name: name
            };
            $('.alert-msg').remove();
            if (name == "") {
                $('#edit-lang-name-' + id).parent().append('<p class="alert-msg">Name field is empty.</p>');
            } else {
                let json = {
                    name: name
                };

                axios.put(url, json)
                    .then(function (response) {
                        $('.close').trigger('click');
                        $("#lang-name-td-" + id).text(response.data.name);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }

            axios.put(url, json)
                .then(function (response) {
                    $('.close').trigger('click');
                    $("#lang-name-td-" + id).text(response.data.name);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    });

    $('[data-target="#categories-colapse"]').click(function () {
        var url = $(this).data('url');
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            data: {},
            success: function (data) {
                $('#categories-colapse-body').html(data);
            },
            async: false
        });
    });

    $(document).on('click', '[data-target="#add-category"]', function (e) {
        var url = $(this).data('url');
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            data: {},
            success: function (data) {
                $('#add-modal-body').html(data);
            },
            async: false
        });
    });

    $(document).on('change', '#add-input-category-img, #add-input-type-img', function (e) {

        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#category-croped-image, #type-croped-image').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    $(document).on('change', '.edit-input-category-img', function (e) {
        let catId = $(this).data('cat-id');
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#edit-category-croped-image-' + catId).attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    // var image2;
    // $(document).on('change', '#add-input-category-img', function (e) {
    //     image2 = $("#to-crop-image-category");
    //     console.log(image2);
    //     if (this.files && this.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function (e) {
    //             console.log(1, e.target.result);
    //             $('#category-croped-image').attr('src', e.target.result);
    //             image2.cropper('replace', e.target.result);
    //             image2.cropper({
    //                 aspectRatio: 1 / 1,
    //                 zoomable: true,
    //                 multiple: true,
    //                 crop(event) {
    //                     originalData = image2.cropper("getCroppedCanvas");
    //                     $('#category-croped-image').attr('src', originalData.toDataURL());
    //                 },
    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);
    //     }
    // });

    // $(document).on('mouseover', '#to-crop-image-category-div', function (e) {
    //     console.log(1);
    //     image2.cropper({
    //         aspectRatio: 1 / 1,
    //         zoomable: true,
    //         multiple: true,
    //         crop(event) {
    //             originalData = image2.cropper("getCroppedCanvas");
    //             $('#category-croped-image').attr('src', originalData.toDataURL());
    //         },
    //     });
    // });

    $(document).on('submit', '#add-category-form', function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let names = [];
        let generalName = $("#add-category-general-name").val();
        let img = $('#category-croped-image').attr('src');
        $('.add-category-name').each(function () {
            names.push({
                name: $(this).val(),
                id: $(this).data('lang-id')
            });
        });

        let json = {
            'img': img,
            'names': names,
            'generalName': generalName
        };
        axios.post(url, json).then(function (response) {
            $('.close').trigger('click');
            $('#gs-table-tbody').append(response.data);
        }).catch(function (error) {
            console.log(error);
        });

        console.log(generalName, names);
    });


    $(document).on('submit', '.edit-category-form', function (e) {
        e.preventDefault();
        let id = $(this).data('category-id');
        let url = $(this).data('url');
        let name = $('#edit-category-name-' + id).val();
        let nameTrans = [];
        let img = $('#edit-category-croped-image-' + id).attr('src');
        $('.edit-category-name-trans-' + id).each(function () {
            nameTrans.push({
                name: $(this).val(),
                id: $(this).data('lang-id')
            });
        });

        let json = {
            'img': img,
            'name': name,
            'nameTrans': nameTrans
        };

        axios.put(url, json).then(function (response) {
            $('.close').trigger('click');
            setTimeout(function () {
                $('#categories-colapse-body').html(response.data);
            }, 100);
        }).catch(function (error) {
            console.log(error);
        });
    });


    $(document).on('click', '.delete-category-btn', function (e) {
        let id = $(this).data('id');
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            $('.close').trigger('click');
            setTimeout(function () {
                $('.category-table-row-' + id).remove();
            }, 100);
        }).catch(function (error) {
            console.log(error);
        });
    });


    $('#add-type-form').submit(function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let catId = $(this).data('cat-id');
        let name = $('#add-type-general-name').val();
        let namesTrans = []
        let img = $('#type-croped-image').attr('src');

        $('.add-type-general-name-trans').each(function () {
            namesTrans.push({
                name: $(this).val(),
                id: $(this).data('lang-id')
            });
        });


        let json = {
            "img": img,
            "catId": catId,
            "name": name,
            "namesTrans": namesTrans
        };
        axios.post(url, json).then(function (response) {
            $('.close').trigger('click');
            $('#types-accordeon-cards').append(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('submit', '#edit-type-form', function (e) {
        e.preventDefault();
        let id = $(this).data('type-id');
        let url = $(this).data('url');
        let name = $('#edit-type-general-name-' + id).val();
        let namesTrans = []

        $('.edit-type-general-name-trans-' + id).each(function () {
            namesTrans.push({
                name: $(this).val(),
                id: $(this).data('lang-id')
            });
        });

        let json = {
            "name": name,
            "namesTrans": namesTrans
        };
        axios.put(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('click', '.delete-type-btn', function (e) {
        let id = $(this).data('id');
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            $('.close').trigger('click');
            $('#type-acordeon-card-' + id).remove();
        }).catch(function (error) {
            console.log(error);
        });
    });


    $(document).on('submit', '#add-propriety-form', function (e) {
        e.preventDefault();
        let typeId = $(this).data('type-id');
        let url = $(this).data('url');
        let name = $('#edit-prop-general-name-' + typeId).val();
        let valIsNumber = 0;
        if ($('#propriety-val-is-number-' + typeId).prop('checked')) {
            valIsNumber = 1;
        }
        let namesTrans = []

        $('.edit-prop-general-name-trans-' + typeId).each(function () {
            namesTrans.push({
                name: $(this).val(),
                id: $(this).data('lang-id')
            });
        });

        let json = {
            "valIsNumber": valIsNumber,
            "typeId": typeId,
            "name": name,
            "namesTrans": namesTrans
        };

        axios.post(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('submit', '.edit-propriety-form', function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let id = $(this).data('propriety-id');
        let name = $('#edit-propriety-name-' + id).val();
        let valIsNumber = 0;
        if ($('#edit-propriety-val-is-number-' + id).prop('checked')) {
            valIsNumber = 1;
        }
        let namesTrans = []

        $('.edit-propriety-name-trans-' + id).each(function () {
            namesTrans.push({
                name: $(this).val(),
                id: $(this).data('propriety-trans-id')
            });
        });

        let json = {
            "valIsNumber": valIsNumber,
            "name": name,
            "namesTrans": namesTrans
        };

        axios.put(url, json).then(function (response) {
            $(".close").trigger('click');
            setTimeout(function () {
                $('#prop-card-' + id).html(response.data);
            }, 100);
        }).catch(function (error) {
            console.log(error);
        });

    });

    $(document).on('click', '.delete-propriety-btn', function (e) {
        let id = $(this).data('id');
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            $('.close').trigger('click');
            $('#prop-card-' + id).remove();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#add-property-val-form').submit(function (e) {
        e.preventDefault();
        let id = $(this).data('prop-id');
        let url = $(this).data('url');
        let name = $('#add-property-val-name').val();
        let namesTrans = [];

        $('.add-property-val-name-trans').each(function () {
            namesTrans.push({
                name: $(this).val(),
                id: $(this).data('lang-id')
            });
        });

        let json = {
            'id': id,
            "name": name,
            "namesTrans": namesTrans
        };

        axios.post(url, json).then(function (response) {
            $('.close').trigger('click');
            $('#prop-val-tbody').append(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });


    $(document).on('submit', '.edit-property-val-form', function (e) {
        e.preventDefault();
        let id = $(this).data('prop-val-id');
        let url = $(this).data('url');
        let name = $('#edit-property-val-name-' + id).val();
        let namesTrans = [];
        $('.edit-property-val-name-trans-' + id).each(function () {
            namesTrans.push({
                name: $(this).val(),
                id: $(this).data('prop-val-trans-id')
            });
        });

        let json = {
            'id': id,
            "name": name,
            "namesTrans": namesTrans
        };

        axios.put(url, json).then(function (response) {
            $('.close').trigger('click');
            $('#prop-val-name-td-' + id).text(name);
            namesTrans.forEach(function (elem) {
                $('#prop-val-name-trans-td-' + elem.id).text(elem.name);
            });
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('click', '.delete-propriety-val-btn', function (e) {
        let id = $(this).data('id');
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            $('.close').trigger('click');
            $('#prop-val-tr-' + id).remove();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#add-blog-post-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let imgBase = $("#croped").attr('src');
        let url = $(this).data('url');
        let postTrans = [];

        $('.add-post-description-trans').each(function () {
            let langId = $(this).data('lang-id');

            let json = {
                langId: langId,
                title: $('#add-post-name-trans-' + langId).val(),
                description: $(this).val()
            };

            postTrans.push(json);
        });

        postTrans = JSON.stringify(postTrans);
        let json = {
            "postTrans": postTrans,
            "img": imgBase
        };

        axios.post(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    var image = $("#croped-image");
    $("#add-post-img, #edit-post-img").change(function () {
        $('#croped').data('changed', 1);
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#croped-image').attr('src', e.target.result);
                image.cropper('replace', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    image.cropper({
        aspectRatio: 900 / 300,
        zoomable: true,
        multiple: true,
        crop(event) {
            originalData = image.cropper("getCroppedCanvas");
            $('#croped').attr('src', originalData.toDataURL());
        },
    });

    $('.delete-post-btn').click(function () {
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#edit-blog-post-form').submit(function (e) {
        e.preventDefault();
        let form = $(this);
        let changed = $('#croped').data('changed');
        let imgBase = $("#croped").attr('src');
        let url = $(this).data('url');
        let postTrans = [];

        $('.edit-post-description-trans').each(function () {
            let langId = $(this).data('lang-id');
            let postId = $(this).data('post-id');

            let json = {
                postId: postId,
                title: $('#edit-post-name-trans-' + langId).val(),
                description: $(this).val()
            };

            postTrans.push(json);
        });

        postTrans = JSON.stringify(postTrans);
        let json = {
            'imgChanged': changed,
            "postTrans": postTrans,
            "img": imgBase
        };

        axios.put(url, json).then(function (response) {
            window.location.href = response.data;
        }).catch(function (error) {
            console.log(error);
        });
    });

    var image1 = $("#to-crop-image-shop");
    $("#add-shop-img, #edit-shop-img").change(function () {
        $('#shop-croped-image').data('changed', 1);
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#shop-croped-image').attr('src', e.target.result);
                image1.cropper('replace', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    image1.cropper({
        aspectRatio: 1 / 1,
        zoomable: true,
        multiple: true,
        crop(event) {
            originalData = image1.cropper("getCroppedCanvas");
            $('#shop-croped-image').attr('src', originalData.toDataURL());
        },
    });

    $('#add-shop-form').submit(function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let name = $('#shop-name').val();
        let email = $('#shop-email').val();
        let phone = $('#shop-phone').val();
        let description = $('#shop-description').val();
        let img = $('#shop-croped-image').attr('src');

        let json = {
            name: name,
            email: email,
            phone: phone,
            description: description,
            img: img
        };

        axios.post(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('.delete-shop-btn').click(function () {
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#edit-shop-form').submit(function (e) {
        e.preventDefault();
        let changed = $('#shop-croped-image').data('changed');
        let url = $(this).data('url');
        let name = $('#shop-name').val();
        let email = $('#shop-email').val();
        let phone = $('#shop-phone').val();
        let description = $('#shop-description').val();
        let img = $('#shop-croped-image').attr('src');

        let json = {
            changed: changed,
            name: name,
            email: email,
            phone: phone,
            description: description,
            img: img
        };

        axios.put(url, json).then(function (response) {
            window.location.href = response.data;
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#add-shop-member-form').submit(function (e) {
        e.preventDefault();
        let id = $(this).data('shop-id');
        let url = $(this).data('url');
        let name = $('#shop-member-name').val();
        let email = $('#shop-member-email').val();
        let phone = $('#shop-member-phone').val();
        let role = $('#shop-member-role option:selected').val();

        let json = {
            id: id,
            name: name,
            email: email,
            phone: phone,
            role: role
        };

        axios.post(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
        console.log(json);
    });


    $('.edit-shop-member-form').submit(function (e) {
        e.preventDefault();
        let memberId = $(this).data('member-id');
        let url = $(this).data('url');
        let name = $('#shop-member-name-' + memberId).val();
        let email = $('#shop-member-email-' + memberId).val();
        let phone = $('#shop-member-phone-' + memberId).val();
        let role = $('#shop-member-role-' + memberId + ' option:selected').val();

        let json = {
            memberId: memberId,
            name: name,
            email: email,
            phone: phone,
            role: role
        };

        axios.put(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('.delete-member-btn').click(function () {
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#add-product-select-type').change(function () {
        let id = $('#add-product-select-type option:selected').val();
        let url = $('#add-product-select-type option:selected').data('url');

        axios.get(url).then(function (response) {
            $('#types-proprieties-row').remove();
            $('#product-types-row').after(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });


    $(document).on('click', '.add-propery-val-btn', function (e) {
        let propId = $(this).data('property-id');
        let url = $(this).data('url');

        axios.get(url).then(function (response) {
            $('#add-propery-val-row-' + propId).before(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('click', '.delete-property-new-val-row', function (e) {
        $(this).parent().parent().remove();
    });

    $("#input-product-images").change(function () {
        imagesPreview(this, '#image-input-preview-galery');
    });

    $(document).on('submit', '#add-product-form', function (e) {
        e.preventDefault();
        let shopId = $(this).data('shop-id');
        let url = $(this).data('url');
        let prodNameDescs = [];
        let typeId = $('#add-product-select-type option:selected').val();
        let json;
        $('.add-product-product-name-and-descript-group').each(function () {
            let langId = $(this).data('lang-id');
            let description;
            $('.add-product-product-description-' + langId).each(function () {
                description = $(this).val();
            });
            json = {
                "langId": langId,
                "name": $('#add-product-product-name-' + langId).val(),
                "description": description
            };
            prodNameDescs.push(json);
        });

        let propValues = [];
        $('.type-property-is-checked').each(function () {
            if ($(this).prop('checked')) {
                let propId = $(this).data('prop-id');
                let valIsNumber = $(this).data('is-number');
                let propVal = [];

                if (valIsNumber == 1) {
                    $('.type-property-val-' + propId).each(function () {
                        let value = $(this).val();
                        let priceDiff = $(this).parent().parent().children('.col-md-3').children('input').val();
                        json = {
                            value: value,
                            priceDiff: priceDiff
                        };
                        propVal.push(json);
                    });
                } else if (valIsNumber == 0) {
                    $('.type-property-val-' + propId + ' option:selected').each(function () {
                        let value = $(this).val();
                        let priceDiff = $(this).parent().parent().parent().children('.col-md-3').children('input').val();
                        json = {
                            value: value,
                            priceDiff: priceDiff
                        };
                        propVal.push(json);
                    });
                }
                json = {
                    propId: propId,
                    propVal: propVal,
                };
                propValues.push(json);
            }
        });

        let basePrice = $('#add-product-product-base-price').val();
        let formData = new FormData(this);
        prodNameDescs = JSON.stringify(prodNameDescs);
        propValues = JSON.stringify(propValues);

        formData.append("prodNameDescs", prodNameDescs);
        formData.append("typeId", typeId);
        formData.append("propValues", propValues);
        formData.append("basePrice", basePrice);
        formData.append("shopId", shopId);

        axios.post(url, formData).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });


    $('.delete-product-confirm-btn').click(function () {
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

});




//  $.ajax({
//      type: 'get',
//      dataType: 'json',
//      url: '/activeDeactiveRow',
//      data: {
//          'id': id,
//          "project":project 
//      },
//      success: function (data) {
//      },
//      async: false
//  });



// $.ajax({
//     type: 'POST',
//     headers: {
//     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     },
//     url: "/changeStatus",
//     data: { _token : $('meta[name="csrf-token"]').attr('content'), 
//         'user':user,
//         'val':val
//     },
//     dataType: "text",
//     success: function(data) {
//     },
//     error: function(data) {      
//     },
// });


// $.ajax({
//     url:url,
//     method:"POST",
//     data:formData,
//     dataType:'JSON',
//     contentType: false,
//     cache: false,
//     processData: false,
//     success:function(data){
//         $('#category-file').val(null);
//     }
// });
