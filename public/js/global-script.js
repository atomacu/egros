$(document).ready(function () {
    $('.change-lang').click(function () {
        let url = $(this).data('url');
        let sysName = $(this).data('sys-name');
        let json = {
            "sysName": sysName
        };

        axios.put(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });
});
