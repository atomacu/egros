$(document).ready(function () {

    $('.prod-property-val').click(function () {
        let val = $(this).data('val');
        let val1 = $(this).data('val1');
        $('#product-cost').text(parseInt(val1) + parseInt(val));
    });

    $(document).on('click', '.wishlist-product-table-row-remove-btn', function (e) {
        let id = $(this).data('prod-id');
        let products = localStorage.getItem("products");
        if (localStorage.getItem("products") !== null) {
            products = products.split(',');

            for (var i = 0; i < products.length; i++) {
                products[i] = parseInt(products[i], 10);
            }

            let prodArrIndex = jQuery.inArray(id, products);
            products.splice(prodArrIndex, 1);
            localStorage.setItem("products", products);
            location.reload();
            $(this).parent().parent().remove();
        }
    });



    $(".prod-add-to-wishlist").click(function () {
        let prodId = $(this).data('prod-id');
        let products = [];
        if (localStorage.getItem("products") === null) {
            localStorage.setItem("products", []);
        }
        if (localStorage.getItem("products") != null) {
            products = localStorage.getItem("products");

        } else {
            products = [];
        }

        if (localStorage.getItem("products") !== null) {
            products = products.split(',');
            if (products[0] == "") {
                products.length = 0

            }

            for (var i = 0; i < products.length; i++) {
                products[i] = parseInt(products[i], 10);
            }

            if (jQuery.inArray(prodId, products) == -1) {
                products.push(prodId);
            }
            localStorage.setItem("products", products);

            location.reload();
        }
    });



    $(document).ready(function () {


        let wishUrl = $('body').data('wish-prod-url');
        let prods = localStorage.getItem("products");
        if (localStorage.getItem("products") !== null) {
            prods = prods.split(',');
            if (prods[0] == "") {
                prods.length = 0

            }

            if (prods.length > 0) {
                $('#wishlist-empty-container').addClass('d-none');
                $('#tbody-wishlist-table').removeClass('d-none');
                $('#app-wishlist-count').text(prods.length);
            }


            for (var i = 0; i < prods.length; i++) {
                prods[i] = parseInt(prods[i], 10);

                $.ajax({
                    type: 'get',
                    dataType: 'json',
                    url: wishUrl,
                    data: {
                        'prodId': prods[i],
                    },
                    success: function (data) {
                        $('#tbody-wishlist-table').append(data);
                    }
                });
            }

            $(".prod-add-to-wishlist").each(function () {
                let prodId = $(this).data('prod-id');
                if (jQuery.inArray(prodId, prods) != -1) {
                    $(this).removeClass('btn-success');
                    if ($(this).is("button")) {
                        $(this).addClass('bg-danger');
                    } else {
                        $(this).children('a').addClass('bg-danger');
                    }
                }
            });

        }

    });

    $('.product-filter-check-property, .product-filter-check-prop-val, #product-filter-price-min-val, #product-filter-price-max-val').change(function () {
        let url = $(this).data('url');
        let property = [];
        let propVal = [];
        let minPrice = $('#product-filter-price-min-val').val();
        let maxPrice = $('#product-filter-price-max-val').val();

        $('.product-filter-check-property').each(function () {
            if ($(this).is(':checked')) {
                property.push($(this).val());
            }
        });

        $('.product-filter-check-prop-val').each(function(){
            if ($(this).is(':checked')) {
                propVal.push($(this).val());
            }
        });

        let json = {
            'property':property,
            'propVal':propVal,
            'minPrice':minPrice,
            'maxPrice':maxPrice,
        };
        
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            data: json,
            success: function (data) {
                $('#tbody-wishlist-table').append(data);
            }
        });
    });


    $('.product-filter-check-property').change(function(){
        let id = $(this).val();
        if ($(this).is(':checked')) {
            $('.product-filter-check-prop-val-'+id).prop('checked', true);
        }else{
            $('.product-filter-check-prop-val-'+id).prop('checked', false);
        }
    });

    $('.product-filter-check-prop-val').change(function(){
        
        let propId = $(this).data('prop-id');
        if ($(this).is(':checked')) {
            $('.product-filter-check-property-'+propId).prop('checked', true);
        }else{
            
            let count=0;
            $('.product-filter-check-prop-val-'+propId).each(function(){
                if ($(this).is(':checked')) {
                    count++;
                }
            });
           
            if(count>0){
                $('.product-filter-check-property-'+propId).prop('checked', true);
            }else{
                $('.product-filter-check-property-'+propId).prop('checked', false);
            }

        }
    });
});
