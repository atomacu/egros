@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-5">
        <img style="width:100%;" src="/images/shops/{{$shop->img}}" alt="">
    </div>
    <div class="col-md-7">
        <div class="row">
            <div class="col-md-12">
                <h2>{{$shop->name}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="description">
                    {{$shop->description}}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <span class="monospaced">{{__('pagination.Phone')}}</span>
            </div>
            <div class="col-md-9">
                <p>{{$shop->phone}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <span class="monospaced">{{__('pagination.Email')}}</span>
            </div>
            <div class="col-md-9">
                <p>{{$shop->email}}</p>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th colspan="6" class="p-0" scope="col">
                        <button class="btn btn-primary btn-block" data-toggle="modal"
                            data-target="#add-shop-member-modal"><i class="fas fa-plus"></i></button>

                        <div class="modal fade" id="add-shop-member-modal" tabindex="-1" role="dialog"
                            aria-labelledby="add-shop-member-modal-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="add-shop-member-modal-label">Add member</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form data-shop-id="{{$shop->id}}"
                                            data-url="{{route('admin-shop-members.store')}}" id="add-shop-member-form">
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <input placeholder="Member name" id="shop-member-name" type="text"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <input placeholder="Member email" id="shop-member-email" type="text"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <input placeholder="Member phone" id="shop-member-phone" type="text"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <select class="form-control" id="shop-member-role" required>
                                                        <option value="" selected disabled>Select member role</option>
                                                        @foreach ($roles as $role)
                                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <button class="btn btn-success btn-block">Add</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Role</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($shop->members as $member)
                <tr>
                    <td>{{$member->name}}</td>
                    <td>{{$member->email}}</td>
                    <td>{{$member->phone}}</td>
                    <td>{{$member->role->name}}</td>
                    <td>
                        <i class="fas fa-edit text-success link" data-toggle="modal"
                            data-target="#edit-member-modal-{{$member->id}}"></i>

                        <div class="modal fade" id="edit-member-modal-{{$member->id}}" tabindex="-1" role="dialog"
                            aria-labelledby="edit-member-modal-{{$member->id}}-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="edit-member-modal-{{$member->id}}-label">Edit member
                                            info</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form data-member-id="{{$member->id}}"
                                            data-url="{{route('admin-shop-members.update',$member->id)}}"
                                            class="edit-shop-member-form">
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <input value="{{$member->name}}" placeholder="Member name"
                                                        id="shop-member-name-{{$member->id}}" type="text"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <input value="{{$member->email}}" placeholder="Member email"
                                                        id="shop-member-email-{{$member->id}}" type="text"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <input value="{{$member->phone}}" placeholder="Member phone"
                                                        id="shop-member-phone-{{$member->id}}" type="text"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <select class="form-control" id="shop-member-role-{{$member->id}}">
                                                        @foreach ($roles as $role)
                                                        @if ($member->role->id==$role->id)
                                                        <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                                        @endif
                                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <button class="btn btn-success btn-block">Edit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <i class="fas fa-trash-alt text-danger link" data-toggle="modal"
                            data-target="#delete-member-modal-{{$member->id}}"></i>

                        <div class="modal fade" id="delete-member-modal-{{$member->id}}" tabindex="-1" role="dialog"
                            aria-labelledby="delete-member-modal-{{$member->id}}-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="delete-member-modal-{{$member->id}}-label">
                                            Are you sure that you want to delete this type?
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row justify-content-center">
                                            <div class="col-md-5">
                                                <button type="button" data-dismiss="modal" aria-label="Close"
                                                    class="btn btn-block btn-success">Cancel</button>
                                            </div>
                                            <div class="col-md-5">
                                                <button data-id="{{$member->id}}" data-url="{{route('admin-shop-members.destroy', $member->id)}}" class="delete-member-btn btn btn-block btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>
@endsection
