@extends('layouts.admin')

@section('content')
<form id="edit-shop-form" data-url="{{route("admin-shop.update",$shop->id)}}">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <input id="shop-name" value="{{$shop->name}}" placeholder="Shop name" type="text" class="form-control" required>
        </div>
    </div>
    <div class="mt-2 row justify-content-center">
        <div class="col-md-10">
            <input id="shop-email" value="{{$shop->email}}" placeholder="Shop email" type="email" class="form-control" required>
        </div>
    </div>
    <div class="mt-2 row justify-content-center">
        <div class="col-md-10">
            <input id="shop-phone" value="{{$shop->phone}}" placeholder="Shop phone" type="text" class="form-control" required>
        </div>
    </div>
    <div class="mt-2 row justify-content-center">
        <div class="col-md-10">
            <textarea id="shop-description" placeholder="Shop description" class="form-control" required>{{$shop->description}}</textarea>
        </div>
    </div>
    <div class="mt-2 row justify-content-center">
        <div class="col-md-10">
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="edit-shop-img">
                    <label class="custom-file-label" for="add-shop-img">Shop image</label>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-2 row justify-content-center">
        <div class="col-md-10">
            <img id="to-crop-image-shop" src="" alt="">
        </div>
    </div>
    <div class="mt-2 row justify-content-center">
        <div class="col-md-10">
            <img data-changed="0" style="width:100%;" id="shop-croped-image" src="/images/shops/{{$shop->img}}" alt="">
        </div>
    </div>
    <div class="mt-2 row justify-content-center">
        <div class="col-md-10">
            <button type="submit" class="btn btn-success btn-block">Add</button>
        </div>
    </div>
</form>
@endsection
