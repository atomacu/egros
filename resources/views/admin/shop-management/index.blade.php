@extends('layouts.admin')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <h3 class="text-dark m-0">
            Shops managment<i class="fas fa-store ml-1"></i>
        </h3>
    </div>
    <div class="col-md-4">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#add-shop-modal"><i
                class="fas fa-plus mr-2"></i>Add shop</button>

        <div class="modal fade" id="add-shop-modal" tabindex="-1" role="dialog" aria-labelledby="add-shop-modal-label"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-shop-modal-label">Add shop</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="add-shop-form" data-url="{{route("admin-shop.store")}}">
                            <div class="row justify-content-center">
                                <div class="col-md-10">
                                    <input id="shop-name" placeholder="Shop name" type="text" class="form-control"
                                        required>
                                </div>
                            </div>
                            <div class="mt-2 row justify-content-center">
                                <div class="col-md-10">
                                    <input id="shop-email" placeholder="Shop email" type="email" class="form-control"
                                        required>
                                </div>
                            </div>
                            <div class="mt-2 row justify-content-center">
                                <div class="col-md-10">
                                    <input id="shop-phone" placeholder="Shop phone" type="text" class="form-control"
                                        required>
                                </div>
                            </div>
                            <div class="mt-2 row justify-content-center">
                                <div class="col-md-10">
                                    <textarea id="shop-description" placeholder="Shop description" class="form-control"
                                        required></textarea>
                                </div>
                            </div>
                            <div class="mt-2 row justify-content-center">
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="add-shop-img" required>
                                            <label class="custom-file-label" for="add-shop-img">Shop image</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mt-2 row justify-content-center">
                                <div class="col-md-10">
                                    <img id="to-crop-image-shop" src="" alt="">
                                </div>
                            </div>
                            <div class="mt-2 row justify-content-center">
                                <div class="col-md-10">
                                    <img style="width:100%;" id="shop-croped-image" src="" alt="">
                                </div>
                            </div>
                            <div class="mt-2 row justify-content-center">
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-success btn-block">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    @foreach ($shops as $shop)
    <div class="mt-2 col-md-3">
        <div class="card">
            <a class="link" href="{{route('admin-shop.show',$shop->id)}}">
                <img src="images/shops/{{$shop->img}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="text-dark card-title">
                        {{$shop->name}}
                    </h5>
                </div>
                <ul class="text-dark list-group list-group-flush">
                    <li class="list-group-item">
                        {{$shop->email}}
                    </li>
                    <li class="list-group-item">
                        {{$shop->phone}}
                    </li>
                    <li class="list-group-item">
                        {{str_limit($shop->description, $limit = 30, $end = '...')}}
                    </li>
                </ul>
            </a>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{route('admin-shop.edit',$shop->id)}}" class="btn btn-primary btn-block">Edit</a>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-danger btn-block" data-toggle="modal"
                            data-target="#delete-shop-modal">Delete</button>

                        <div class="modal fade" id="delete-shop-modal" tabindex="-1" role="dialog"
                            aria-labelledby="delete-shop-modal-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="delete-shop-modal-label">
                                            Are you sure that you want to delete this shop?
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row justify-content-center">
                                            <div class="col-md-5">
                                                <button type="button" data-dismiss="modal" aria-label="Close"
                                                    class="btn btn-block btn-success">Cancel</button>
                                            </div>
                                            <div class="col-md-5">
                                                <button data-url="{{route('admin-shop.destroy',$shop->id)}}" class="delete-shop-btn btn btn-block btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
