@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin-general-settings.index')}}">General settings</a></li>
        <li class="breadcrumb-item"><a href="">{{$propriety->type->category->name}}</a></li>
        <li class="breadcrumb-item"><a href="">{{$propriety->type->name}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$propriety->name}}</li>
    </ol>
</nav>
<table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th class="p-0" colspan="{{count($langs)+3}}">
                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#add-propriety-val-modal"><i
                        class="fas fa-plus"></i></button>

                <div class="modal fade" id="add-propriety-val-modal" tabindex="-1" role="dialog"
                    aria-labelledby="add-propriety-val-modal-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="add-propriety-val-modal-label">Add proprty val</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form data-prop-id="{{$propriety->id}}"
                                    data-url="{{route('admin-propriety-val.store')}}" id="add-property-val-form">
                                    <div class="row justify-content-center">
                                        <div class="col-md-10">
                                            <input placeholder="Property value general name" id="add-property-val-name"
                                                type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    @foreach ($langs as $lang)
                                    <div class="row mt-2 justify-content-center">
                                        <div class="col-md-10">
                                            <input data-lang-id="{{$lang->id}}" placeholder="{{$lang->name}}"
                                                type="text" class="add-property-val-name-trans form-control" required>
                                        </div>
                                    </div>
                                    @endforeach

                                    <div class="row mt-2 justify-content-center">
                                        <div class="col-md-10">
                                            <button type="submit" class="btn btn-success btn-block">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </th>
        </tr>
        <tr>
            <th class="w-50 text-center" scope="col">Value</th>
            @foreach ($langs as $lang)
            <th class="w-12-5 text-center" scope="col">{{$lang->name}}</th>
            @endforeach
            <th class="w-10 text-center" scope="col">Edit</th>
            <th class="w-10 text-center" scope="col">Delete</th>
        </tr>
    </thead>
    <tbody id="prop-val-tbody">
        @foreach ($propriety->propVal as $propVal)
        <tr id="prop-val-tr-{{$propVal->id}}">
            <td class="text-center" id="prop-val-name-td-{{$propVal->id}}">{{$propVal->name}}</td>
            @foreach ($langs as $lang)
            <td class="text-center" id="prop-val-name-trans-td-{{$propVal->getProprietyValTrans($lang->id)['id']}}">
                {{$propVal->getProprietyValTrans($lang->id)['name']}}</td>
            @endforeach
            <td class="text-center">
                <i class="fas fa-edit text-success link" data-toggle="modal"
                    data-target="#edit-prop-val-modal-{{$propVal->id}}"></i>

                <div class="modal fade" id="edit-prop-val-modal-{{$propVal->id}}" tabindex="-1" role="dialog"
                    aria-labelledby="edit-prop-val-modal-{{$propVal->id}}-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="edit-prop-val-modal-{{$propVal->id}}-label">Edit
                                    property values</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form data-prop-val-id="{{$propVal->id}}"
                                    data-url="{{route('admin-propriety-val.update',$propVal->id)}}"
                                    class="edit-property-val-form">
                                    <div class="row justify-content-center">
                                        <div class="col-md-10">
                                            <input placeholder="Property value general name" value="{{$propVal->name}}"
                                                id="edit-property-val-name-{{$propVal->id}}" type="text"
                                                class="form-control" required>
                                        </div>
                                    </div>
                                    @foreach ($langs as $lang)
                                    <div class="row mt-2 justify-content-center">
                                        <div class="col-md-10">
                                            <input
                                                data-prop-val-trans-id="{{$propVal->getProprietyValTrans($lang->id)['id']}}"
                                                value="{{$propVal->getProprietyValTrans($lang->id)['name']}}"
                                                placeholder="{{$lang->name}}" type="text"
                                                class="edit-property-val-name-trans-{{$propVal->id}} form-control"
                                                required>
                                        </div>
                                    </div>
                                    @endforeach

                                    <div class="row mt-2 justify-content-center">
                                        <div class="col-md-10">
                                            <button type="submit" class="btn btn-success btn-block">Edit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </td>
            <td class="text-center">
                <i class="fas fa-trash-alt text-danger link" data-toggle="modal"
                    data-target="#delete-prop-val-modal-{{$propVal->id}}"></i>

                <div class="modal fade" id="delete-prop-val-modal-{{$propVal->id}}" tabindex="-1" role="dialog"
                    aria-labelledby="delete-prop-val-modal-{{$propVal->id}}-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="delete-prop-val-modal-{{$propVal->id}}-label">
                                    Are you sure that you want to delete this propriety value?
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row justify-content-center">
                                    <div class="col-md-5">
                                        <button type="button" data-dismiss="modal" aria-label="Close"
                                            class="btn btn-block btn-success">Cancel</button>
                                    </div>
                                    <div class="col-md-5">
                                        <button data-id="{{$propVal->id}}"
                                            data-url="{{route('admin-propriety-val.destroy',$propVal->id)}}"
                                            class="delete-propriety-val-btn btn btn-block btn-danger">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
