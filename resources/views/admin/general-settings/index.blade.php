@extends('layouts.admin')

@section('content')
<div class="accordion" id="general-settings-acordeon">
    <div class="card">
        <div class="card-header" id="langs">
            <h2 class="mb-0">
                <a class="h4 link" data-toggle="collapse" data-target="#lang-colapse" aria-expanded="true"
                    aria-controls="lang-colapse">
                    Languages<i class="fas fa-language ml-1"></i>
                </a>
            </h2>
        </div>

        <div id="lang-colapse" class="collapse" aria-labelledby="langs" data-parent="#general-settings-acordeon">
            <div class="card-body">
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center p-0" colspan="4" scope="col">
                                <button class="btn btn-primary btn-block" data-toggle="modal"
                                    data-target="#add-lang-modal">
                                    <i class="fas fa-plus"></i>
                                </button>


                                <div class="modal fade" id="add-lang-modal" tabindex="-1" role="dialog"
                                    aria-labelledby="add-lang-modal-label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="add-lang-modal-label">Add lang</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="POST" id="add-lang-form"
                                                    data-url="{{route('admin-langs.store')}}">
                                                    @csrf
                                                    <div class="container">
                                                        <div class="row mb-2 justify-content-center">
                                                            <div class="col-md-10">
                                                                <input type="text" id="add-lang-name" placeholder="Lang name" class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <div class="row justify-content-center">
                                                            <div class="col-md-10">
                                                                <button type="submit" class="btn btn-success btn-block">Add</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th class="w-10 text-center" scope="col">#</th>
                            <th class="text-center" scope="col">Lang</th>
                            <th class="w-5 text-center" scope="col">Edit</th>
                            <th class="w-5 text-center" scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody id="add-lang-modal-tbody">
                        @foreach ($langs as $i=>$lang)
                            <tr id="lang-table-row-{{$lang->id}}">
                                <th class="text-center" scope="row">{{$i+1}}</th>
                                <td id="lang-name-td-{{$lang->id}}" class="text-center">{{$lang->name}}</td>
                                <td class="text-center"> 
                                    <i class="fas fa-pencil-alt text-success" data-toggle="modal" data-target="#edit-lang-general-settings-{{$lang->id}}"></i>
                                
                                    <div class="modal fade" id="edit-lang-general-settings-{{$lang->id}}" tabindex="-1"
                                        role="dialog" aria-labelledby="edit-lang-general-settings-label-{{$lang->id}}"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"
                                                        id="edit-lang-general-settings-label-{{$lang->id}}">Edit lang</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" class="edit-lang-form" data-id="{{$lang->id}}"
                                                        data-url="{{route('admin-langs.update',$lang->id)}}">
                                                        @csrf
                                                        <div class="container">
                                                            <div class="row mb-2 justify-content-center">
                                                                <div class="col-md-10">
                                                                    <input type="text" id="edit-lang-name-{{$lang->id}}"
                                                                        value="{{$lang->name}}" placeholder="Lang name"
                                                                        class="form-control" required>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="col-md-10">
                                                                    <button type="submit"
                                                                        class="btn btn-success btn-block">Save</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#delete-lang-general-settings-{{$lang->id}}"></i>
                                
                                    <div class="modal fade" id="delete-lang-general-settings-{{$lang->id}}" tabindex="-1"
                                        role="dialog" aria-labelledby="delete-lang-general-settings-label-{{$lang->id}}"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"
                                                        id="delete-lang-general-settings-label-{{$lang->id}}">
                                                        Are you sure that you want to delete this language?
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="container">
                                                        <div class="row justify-content-center">
                                                            <div class="col-md-5">
                                                                <button type="button" data-dismiss="modal"
                                                                    aria-label="Close"
                                                                    class="btn btn-success btn-block">Cancel</button>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <button data-id="{{$lang->id}}"
                                                                    data-url="{{route('admin-langs.destroy',$lang->id)}}"
                                                                    class="delete-lang-btn btn btn-danger btn-block">Delete</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



    <div class="card">
        <div class="card-header" id="categories">
            <h2 class="mb-0">
                <a data-url="{{route('admin-category.index')}}" class="h4 link" data-toggle="collapse" data-target="#categories-colapse" aria-expanded="true" aria-controls="categories-colapse">
                    Categories<i class="fas fa-box ml-1"></i>
                </a>
            </h2>
        </div>
        <div id="categories-colapse" class="collapse" aria-labelledby="categories" data-parent="#general-settings-acordeon">
            <div id="categories-colapse-body" class="card-body">

            </div>
        </div>
    </div>
</div>
@endsection
