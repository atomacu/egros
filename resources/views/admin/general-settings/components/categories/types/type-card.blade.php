<div id="type-acordeon-card-{{$type->id}}" class="card">
    <div class="card-header" id="headingOne">
        <div class="row">
            <div class="col-md-10">
                <h2 class="mb-0">
                    <button class="btn btn-block text-left" type="button" data-toggle="collapse"
                        data-target="#type-collapse-{{$type->id}}" aria-expanded="true"
                        aria-controls="type-collapse-{{$type->id}}">
                        <div class="row">
                            <div class="col-md-2">
                                <img style="width:100%;" src="/images/types/{{$type->img}}" alt="">
                            </div>
                            <div class="col-md-10">
                                {{$type->name}}
                                <div class="row">
                                    @foreach ($langs as $lang)
                                    <div class="text-left col-md-12">
                                        <b>{{$lang->name}} :</b> {{$type->getTypeTrans($lang->system_name)['name']}}
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </button>
                </h2>
            </div>
            <div class="col-md-1 text-center">
                <i class="fas fa-edit text-success link" data-toggle="modal"
                    data-target="#edit-type-modal-{{$type->id}}"></i>

                <div class="modal fade" id="edit-type-modal-{{$type->id}}" tabindex="-1" role="dialog"
                    aria-labelledby="edit-type-modal-{{$type->id}}-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="edit-type-modal-{{$type->id}}-label">Edit type</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form data-type-id="{{$type->id}}"
                                    data-url="{{route('admin-types.update', $type->id)}}" id="edit-type-form">
                                    <div class="row justify-content-center">
                                        <div class="col-md-10">
                                            <input value="{{$type->name}}" placeholder="Type general name"
                                                id="edit-type-general-name-{{$type->id}}" type="text"
                                                class="form-control" required>
                                        </div>
                                    </div>
                                    @foreach ($langs as $lang)
                                    <div class="row mt-2 justify-content-center">
                                        <div class="col-md-10">
                                            <input data-lang-id="{{$lang->id}}"
                                                value="{{$type->getTypeTrans($lang->id)['name']}}"
                                                placeholder="{{$lang->name}}" type="text"
                                                class="edit-type-general-name-trans-{{$type->id}} form-control"
                                                required>
                                        </div>
                                    </div>
                                    @endforeach

                                    <div class="row mt-2 justify-content-center">
                                        <div class="col-md-10">
                                            <button class="btn btn-success btn-block">Edit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-1 text-center">
                <i class="fas fa-trash-alt text-danger link" data-toggle="modal"
                    data-target="#delete-type-modal-{{$type->id}}"></i>

                <div class="modal fade" id="delete-type-modal-{{$type->id}}" tabindex="-1" role="dialog"
                    aria-labelledby="delete-type-modal-{{$type->id}}-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="delete-type-modal-{{$type->id}}-label">
                                    Are you sure that you want to delete this type?
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row justify-content-center">
                                    <div class="col-md-5">
                                        <button type="button" data-dismiss="modal" aria-label="Close"
                                            class="btn btn-block btn-success">Cancel</button>
                                    </div>
                                    <div class="col-md-5">
                                        <button data-id="{{$type->id}}"
                                            data-url="{{route('admin-types.destroy', $type->id)}}"
                                            class="delete-type-btn btn btn-block btn-danger">Delete</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div id="type-collapse-{{$type->id}}" class="collapse" aria-labelledby="headingOne"
        data-parent="#types-accordeon-cards">
        <div class="card-body">

            <div class="row">
                <div class="col-md-3">
                    <div class="card h-100">
                        <div class="bg-primary link proproety-add-card card-body text-center" data-toggle="modal"
                            data-target="#add-propriety-modal-{{$type->id}}">
                            <i class="fas fa-plus h1"></i>
                        </div>

                        <div class="modal fade" id="add-propriety-modal-{{$type->id}}" tabindex="-1" role="dialog"
                            aria-labelledby="add-propriety-modal-{{$type->id}}-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="add-propriety-modal-{{$type->id}}-label">Add
                                            propriety</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form data-url="{{route('admin-propriety.store')}}" id="add-propriety-form"
                                            data-type-id="{{$type->id}}">
                                            <div class="row justify-content-center">
                                                <div class="col-md-10">
                                                    <input placeholder="Propriety name"
                                                        id="edit-prop-general-name-{{$type->id}}"
                                                        class="form-control" type="text">
                                                </div>
                                            </div>
                                            @foreach ($langs as $lang)
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <input data-lang-id="{{$lang->id}}"
                                                        placeholder="{{$lang->name}}"
                                                        class="edit-prop-general-name-trans-{{$type->id}} form-control"
                                                        type="text">
                                                </div>
                                            </div>
                                            @endforeach
                                            <div class="my-3 row justify-content-center">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="1"
                                                        id="propriety-val-is-number-{{$type->id}}">
                                                    <label class="form-check-label" for="propriety-val-is-number">
                                                        Property value is an undefined number
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row mt-2 justify-content-center">
                                                <div class="col-md-10">
                                                    <button class="btn btn-success btn-block">Add</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach ($type->proprieties as $propriety)
                <div id="prop-card-{{$propriety->id}}" class="col-md-3">
                    <div class="card h-100">
                        <div class="card-body pt-0 px-0 text-center">
                            <div class="row">
                                <div class="col-md-12">
                                    @if ($propriety->val_is_number==0)
                                    <a class="btn btn-primary btn-block"
                                        href="{{route('admin-propriety.show',$propriety->id)}}">
                                        <i class="fas fa-plus"></i>
                                    </a>
                                    @elseif($propriety->val_is_number==1)
                                    Values ​​for this property is given by the shop administrator.
                                    @endif
                                </div>
                            </div>
                            <div class="px-2 row">
                                <div class="col-md-12">
                                    <h2 id="card-prop-name-{{$propriety->id}}">{{$propriety->name}}</h2>
                                </div>
                            </div>
                            <div class="px-4 row">
                                @foreach ($langs as $lang)
                                <div class="text-left col-md-12">
                                    <b>{{$lang->name}} :</b>
                                    <span
                                        id="card-prop-trans-{{$propriety->getProprietyTrans($lang->system_name)['id']}}">{{$propriety->getProprietyTrans($lang->system_name)['name']}}</span>
                                </div>
                                @endforeach
                            </div>



                            <div class="px-2 row justify-content-center">
                                <div class="col-md-4">
                                    <i class="fas fa-edit text-success link" data-toggle="modal"
                                        data-target="#edit-propriety-modal-{{$propriety->id}}"></i>

                                    <div class="modal fade" id="edit-propriety-modal-{{$propriety->id}}"
                                        tabindex="-1" role="dialog"
                                        aria-labelledby="edit-propriety-modal-{{$propriety->id}}-label"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"
                                                        id="edit-propriety-modal-{{$propriety->id}}-label">Edit
                                                        propriety</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form
                                                        data-url="{{route('admin-propriety.update',$propriety->id)}}"
                                                        data-propriety-id="{{$propriety->id}}"
                                                        class="edit-propriety-form">
                                                        <div class="row justify-content-center mb-2">
                                                            <div class="col-md-10">
                                                                <input type="text" value="{{$propriety->name}}"
                                                                    class="form-control"
                                                                    id="edit-propriety-name-{{$propriety->id}}">
                                                            </div>
                                                        </div>
                                                        @foreach ($langs as $lang)
                                                        <div class="row justify-content-center mb-2">
                                                            <div class="col-md-10">
                                                                <input type="text" placeholder="{{$lang->name}}"
                                                                    value="{{$propriety->getProprietyTrans($lang->id)['name']}}"
                                                                    data-propriety-trans-id="{{$propriety->getProprietyTrans($lang->id)['id']}}"
                                                                    class="edit-propriety-name-trans-{{$propriety->id}} form-control">
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        <div class="my-3 row justify-content-center">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox"
                                                                    value="1"
                                                                    id="edit-propriety-val-is-number-{{$propriety->id}}"
                                                                    @if($propriety->val_is_number==1) checked
                                                                @endif>
                                                                <label class="form-check-label"
                                                                    for="edit-propriety-val-is-number">
                                                                    Property value is an undefined number
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row justify-content-center mb-2">
                                                            <div class="col-md-10">
                                                                <button type="submit"
                                                                    class="btn btn-success btn-block">Edit</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <i class="fas fa-trash-alt text-danger link" data-toggle="modal"
                                        data-target="#delete-prop-modal-{{$propriety->id}}"></i>

                                    <div class="modal fade" id="delete-prop-modal-{{$propriety->id}}" tabindex="-1"
                                        role="dialog" aria-labelledby="delete-prop-modal-{{$propriety->id}}-label"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title"
                                                        id="delete-prop-modal-{{$propriety->id}}-label">
                                                        Are you sure that you want to delete this propriety?
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row justify-content-center">
                                                        <div class="col-md-5">
                                                            <button type="button" data-dismiss="modal"
                                                                aria-label="Close"
                                                                class="btn btn-block btn-success">Cancel</button>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <button data-id="{{$propriety->id}}"
                                                                data-url="{{route('admin-propriety.destroy',$propriety->id)}}"
                                                                class="delete-propriety-btn btn btn-block btn-danger">Delete</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>