<div>
    <form id="add-category-form" data-url="{{route('admin-category.store')}}" method="post">
        <div class="row mb-2 justify-content-center">
            <div class="col-md-10">
                <input type="text" value="" id="add-category-general-name" placeholder="Category general name"
                    class="form-control" required>
            </div>
        </div>
        @foreach ($langs as $lang)
        <div class="row mb-2 justify-content-center">
            <div class="col-md-10">
                <input data-lang-id="{{$lang->id}}" type="text" placeholder="Category name in {{$lang->name}}"
                    data-lang="" class="add-category-name form-control" required>
            </div>
        </div>
        @endforeach
        <div class="row mb-2 justify-content-center">
            <div class="col-md-10">
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="add-input-category-img" required>
                        <label class="custom-file-label" for="add-input-category-img">Category image</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="my-2 row justify-content-center">
            <div class="col-md-10">
                <img style="width:100%;" id="category-croped-image" src="" alt="">
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <button type="submit" class="btn btn-success btn-block">Add</button>
            </div>
        </div>
    </form>
</div>
