<table class="table">
    <thead class="thead-light">
        <tr>
            <th colspan="{{count($langs)+6}}" class="p-0" scope="col">
                <button data-url="{{route('admin-category.create')}}" class="btn btn-primary btn-block"
                    data-toggle="modal" data-target="#add-category">
                    <i class="fas fa-plus"></i>
                </button>

                <div class="modal fade" id="add-category" tabindex="-1" role="dialog"
                    aria-labelledby="add-category-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="add-category-label">Add category</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div id="add-modal-body" class="modal-body">
                            </div>
                        </div>
                    </div>
                </div>
            </th>
        </tr>
        <tr>
            <th class="w-5 text-center" scope="col">#</th>
            <th class="w-5 text-center" scope="col">Image</th>
            <th class="w-15 text-center" scope="col">Category link</th>
            <th class="w-20 text-center" scope="col">General name</th>
            @foreach ($langs as $lang)
            <th class="text-center" scope="col">{{$lang->name}}</th>
            @endforeach
            <th class="w-5 text-center" scope="col">Edit</th>
            <th class="w-5 text-center" scope="col">Delete</th>
        </tr>
    </thead>
    <tbody id="gs-table-tbody">
        @foreach ($categories as $i=>$category)
        <tr class="category-table-row-{{$category->id}}">
            <th class="text-center" scope="row">{{$i+1}}</th>
            <th class="text-center pt-1" scope="row">
                <img style="width:100%;" src="/images/categories/{{$category->img}}" alt="">
            </th>
            <td class="text-center">
                <a href="{{route('admin-category.show',$category->id)}}">
                    <i class="fas fa-link"></i>
                </a>
            </td>
            <td class="text-center">{{$category->name}}</td>
            @foreach ($langs as $lang)
            <td class="text-center" scope="col">{{$category->getCategoryTrans($lang->system_name)['name']}}</td>
            @endforeach
            <td class="text-center">
                <i class="fas fa-edit text-success" data-toggle="modal"
                    data-target="#edit-category-gs-modal-{{$category->id}}"></i>

                <div class="modal fade" id="edit-category-gs-modal-{{$category->id}}" tabindex="-1" role="dialog"
                    aria-labelledby="edit-category-gs-modal-{{$category->id}}-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="edit-category-gs-modal-{{$category->id}}-label">Edit
                                    category</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form data-url="{{route('admin-category.update',$category->id)}}"
                                    data-category-id="{{$category->id}}" class="edit-category-form">
                                    <div class="row justify-content-center">
                                        <div class="col-md-10">
                                            <input type="text" class="form-control"
                                                id="edit-category-name-{{$category->id}}" value="{{$category->name}}"
                                                required>
                                        </div>
                                    </div>
                                    @foreach ($langs as $lang)
                                    <div class="row mt-2 justify-content-center">
                                        <div class="col-md-10">
                                            <input placeholder="{{$lang->name}}" data-lang-id="{{$lang->id}}"
                                                type="text"
                                                class="form-control edit-category-name-trans-{{$category->id}}"
                                                value="{{$category->getCategoryTrans($lang->system_name)['name']}}" required>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="row my-2 justify-content-center">
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="edit-input-category-img custom-file-input" data-cat-id="{{$category->id}}">
                                                    <label class="custom-file-label" for="add-input-category-img">Category image</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="my-2 row justify-content-center">
                                        <div class="col-md-10">
                                            <img style="width:100%;" id="edit-category-croped-image-{{$category->id}}" src="" alt="">
                                        </div>
                                    </div>
                                    <div class="row mt-2 justify-content-center">
                                        <div class="col-md-10">
                                            <button type="submit" class="btn-block btn btn-success">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <td class="text-center">
                <i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#delete-category-modal"></i>

                <div class="modal fade" id="delete-category-modal" tabindex="-1" role="dialog"
                    aria-labelledby="delete-category-modal-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-center" id="delete-category-modal-label">Are you sure that
                                    you want to delete this category ?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row justify-content-center">
                                    <div class="col-md-5">
                                        <button type="button" data-dismiss="modal" aria-label="Close"
                                            class="btn btn-block btn-success">Cancel</button>
                                    </div>
                                    <div class="col-md-5">
                                        <button data-id="{{$category->id}}"
                                            data-url="{{route('admin-category.destroy',$category->id)}}"
                                            class="delete-category-btn btn btn-block btn-danger">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
