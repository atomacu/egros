<tr id="lang-table-row-{{$lang->id}}">
    <th class="text-center" scope="row">{{$i+1}}</th>
    <td id="lang-name-td-{{$lang->id}}" class="text-center">{{$lang->name}}</td>
    <td class="text-center"> 
        <i class="fas fa-pencil-alt text-success" data-toggle="modal" data-target="#edit-lang-general-settings-{{$lang->id}}"></i>
    
        <div class="modal fade" id="edit-lang-general-settings-{{$lang->id}}" tabindex="-1"
            role="dialog" aria-labelledby="edit-lang-general-settings-label-{{$lang->id}}"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="edit-lang-general-settings-label-{{$lang->id}}">Edit lang</h5>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" class="edit-lang-form" data-id="{{$lang->id}}"
                            data-url="{{route('admin-langs.update',$lang->id)}}">
                            @csrf
                            <div class="container">
                                <div class="row mb-2 justify-content-center">
                                    <div class="col-md-10">
                                        <input type="text" id="edit-lang-name-{{$lang->id}}"
                                            value="{{$lang->name}}" placeholder="Lang name"
                                            class="form-control" required>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-10">
                                        <button type="submit"
                                            class="btn btn-success btn-block">Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </td>
    <td class="text-center">
        <i class="fas fa-trash-alt text-danger" data-toggle="modal" data-target="#delete-lang-general-settings-{{$lang->id}}"></i>
    
        <div class="modal fade" id="delete-lang-general-settings-{{$lang->id}}" tabindex="-1"
            role="dialog" aria-labelledby="delete-lang-general-settings-label-{{$lang->id}}"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="delete-lang-general-settings-label-{{$lang->id}}">
                            Are you sure that you want to delete this language?
                        </h5>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-md-5">
                                    <button type="button" data-dismiss="modal"
                                        aria-label="Close"
                                        class="btn btn-success btn-block">Cancel</button>
                                </div>
                                <div class="col-md-5">
                                    <button data-id="{{$lang->id}}"
                                        data-url="{{route('admin-langs.destroy',$lang->id)}}"
                                        class="delete-lang-btn btn btn-danger btn-block">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </td>
</tr>