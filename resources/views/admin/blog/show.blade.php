@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-lg-8">
        <h1 class="mt-4">
            {{$post->postTrans(Auth::user()->default_lang)['title']}}
        </h1>
        <p class="lead">
            by {{$post->user->name}}
        </p>
        <hr>
        <p>
            Posted on {{date_format($post->created_at, 'd.m.y , g:i a')}}
        </p>
        <hr>
        <img style="width:100%" class="img-fluid rounded" src="/images/cover/{{$post->img}}" alt="">
        <hr>
        {!! $post->postTrans(Auth::user()->default_lang)['description']!!}
        <hr>
    </div>
    <div class="col-md-4">
        <div class="card my-4">
            <h5 class="card-header">Search</h5>
            <div class="card-body">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
