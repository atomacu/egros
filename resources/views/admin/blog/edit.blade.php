@extends('layouts.admin')

@section('content')
<form id="edit-blog-post-form" data-url="{{route('admin-blog.update',$post->id)}}">
    <div class="row justify-content-center mb-2">
        <div class="col-md-10">
            <div class="custom-file">
                <input type="file" name="cover_post" class="custom-file-input" id="edit-post-img">
                <label class="custom-file-label" for="edit-post-img">Choose image</label>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-2">
        <div class="col-md-10">
            <img style="width:100%;" src="" id="croped-image" alt="">
        </div>
    </div>

    <div class="row justify-content-center mb-2">
        <div class="col-md-10">
            <img data-changed="0" style="width:100%;" src="/images/cover/{{$post->img}}" id="croped" alt="">
        </div>
    </div>

    @foreach ($langs as $i=>$lang)
    @if($i==0)
    <hr>
    @endif
    <div class="row justify-content-center mb-2">
        <div class="col-md-10">
            <input value="{{$post->postTrans($lang->system_name)['title']}}" placeholder="{{$lang->name}}"
                id="edit-post-name-trans-{{$lang->id}}" type="text" class="form-control">
        </div>
    </div>
    <div class="row justify-content-center mb-2">
        <div class="col-md-10">
            <textarea name="content" data-post-id="{{$post->postTrans($lang->system_name)['id']}}"
                data-lang-id="{{$lang->id}}" class="edit-post-description-trans"
                id="editor-{{$i}}">{!!$post->postTrans($lang->system_name)['description']!!}</textarea>
        </div>
    </div>
    <hr>
    @endforeach

    <div class="row justify-content-center">
        <div class="col-md-10">
            <button type="submit" class="btn btn-success btn-block"><i class="far fa-save mr-2"></i>Save</button>
        </div>
    </div>
</form>
@endsection
