@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h3 class="text-dark m-0">
            <i class="far fa-newspaper mr-2"></i>
            {{-- Blog --}}
            {{ __('auth.failed')}}
        </h3>
    </div>
    <div class="col-md-6">
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#add-post-modal"><i
                class="fas fa-plus mr-2"></i>Add post</button>

        <div class="modal fade" id="add-post-modal" tabindex="-1" role="dialog" aria-labelledby="add-post-modal-label"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-post-modal-label">Add post</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="add-blog-post-form" data-url="{{route('admin-blog.store')}}">
                            <div class="row justify-content-center mb-2">
                                <div class="col-md-10">
                                    <div class="custom-file">
                                        <input type="file" name="cover_post" class="custom-file-input"
                                            id="add-post-img">
                                        <label class="custom-file-label" for="add-post-img">Choose image</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center mb-2">
                                <div class="col-md-10">
                                    <img style="width:100%;" src="" id="croped-image" alt="">
                                </div>
                            </div>

                            <div class="row justify-content-center mb-2">
                                <div class="col-md-10">
                                    <img style="width:100%;" src="" id="croped" alt="">
                                </div>
                            </div>

                            @foreach ($langs as $i=>$lang)
                            @if($i==0)
                            <hr>
                            @endif
                            <div class="row justify-content-center mb-2">
                                <div class="col-md-10">
                                    <input placeholder="{{$lang->name}}" id="add-post-name-trans-{{$lang->id}}"
                                        type="text" class="form-control">
                                </div>
                            </div>
                            <div class="row justify-content-center mb-2">
                                <div class="col-md-10">
                                    <textarea name="content" data-lang-id="{{$lang->id}}"
                                        class="add-post-description-trans" id="editor-{{$i}}"></textarea>
                                </div>
                            </div>
                            <hr>
                            @endforeach

                            <div class="row justify-content-center">
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-success btn-block">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="row">
    @foreach ($posts as $post)
    <div class="col-md-6">
        <div class="card mb-4">
            <a class="link text-dark" href="{{route('admin-blog.show',$post->id)}}">
                <img class="card-img-top" src="/images/cover/{{$post->img}}" alt="Card image cap">
            </a>
            <div class="pt-1 card-body text-center">
                <a class="link text-dark" href="{{route('admin-blog.show',$post->id)}}">

                    <h2 class="card-title">{{$post->postTrans(Auth::user()->default_lang)['title']}}</h2>
                    <p class="card-text">
                        {!! str_limit(strip_tags($post->postTrans(Auth::user()->default_lang)['description']), $limit =
                        65, $end = '...')!!}
                    </p>
                </a>
                <div class="mt-3 row">
                    <div class="col-md-6">
                        <a href="{{route('admin-blog.edit',$post->id)}}" class="btn btn-primary btn-block">
                                <i class="fas fa-edit mr-2"></i>Edit
                        </a>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-danger btn-block" data-toggle="modal" data-target="#delete-post-modal">
                                <i class="fas fa-trash-alt mr-2"></i>Delete
                        </button>

                        <div class="modal fade" id="delete-post-modal" tabindex="-1" role="dialog"
                            aria-labelledby="delete-post-modal-label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="delete-post-modal-label">Are you sure that you
                                            want to delete this post?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row justify-content-center">
                                            <div class="col-md-5">
                                                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-block btn-success">Cancel</button>
                                            </div>
                                            <div class="col-md-5">
                                                <button data-id="{{$post->id}}" data-url="{{route('admin-blog.destroy',$post->id)}}" class="delete-post-btn btn btn-block btn-danger">
                                                    Delete
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @endforeach
</div>


@endsection
