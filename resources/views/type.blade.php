@extends('layouts.app')

@section('content')
<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-md-12 p-0">
            <img src="https://picsum.photos/id/583/1800/350" class="img-responsive category-img">
            <div class="mb-5 pb-5 carousel-caption">
                <h1>
                    {{$type->getTypeTrans($langSysName)['name']}}
                </h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
                    <div class="shop__sidebar">
                        <aside class="wedget__categories poroduct--cat">
                            <div class="accordion" id="filter-props-acordeon">
                                @foreach ($type->proprieties as $i=>$propriety)
                                <div class="card border-0">
                                    <div class="p-2" id="prop-card-{{$propriety->id}}">
                                        <h2 class="m-0">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <input data-url="{{route('getFilterResult',$langSysName)}}"
                                                        type="checkbox"
                                                        class="product-filter-check-property product-filter-check-property-{{$propriety->id}}"
                                                        value="{{$propriety->id}}" id="property-{{$propriety->id}}">
                                                    <button class="text-left p-0 btn dropdown-toggle collapsed"
                                                        type="button" data-toggle="collapse"
                                                        data-target="#prop-card-{{$propriety->id}}-collapse"
                                                        aria-expanded="false"
                                                        aria-controls="prop-card-{{$propriety->id}}-collapse">
                                                        {{$propriety->name}}
                                                    </button>
                                                </div>
                                            </div>
                                        </h2>
                                    </div>
                                    @if($propriety->val_is_number==0)
                                    <div id="prop-card-{{$propriety->id}}-collapse" class="collapse"
                                        aria-labelledby="prop-card-{{$propriety->id}}"
                                        data-parent="#filter-props-acordeon">
                                        <div class="pt-0 card-body">
                                            @foreach ($propriety->propVal as $propVal)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input data-url="{{route('getFilterResult',$langSysName)}}"
                                                        type="checkbox" data-prop-id="{{$propriety->id}}"
                                                        class="product-filter-check-prop-val-{{$propriety->id}} product-filter-check-prop-val"
                                                        value="{{$propVal->id}}"
                                                        id="property-{{$propriety->id}}-propval-{{$propVal->id}}">
                                                    {{$propVal->name}}
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @elseif($propriety->val_is_number==1)
                                    <div id="prop-card-{{$propriety->id}}-collapse" class="collapse"
                                        aria-labelledby="prop-card-{{$propriety->id}}"
                                        data-parent="#filter-props-acordeon">
                                        <div class="pt-0 card-body">
                                            @foreach ($propriety->propProducts as $propProduct)
                                            {{$propProduct->value}}
                                                {{-- @foreach ($propProduct->prodPropValues as $propValue) --}}
                                           
                                                {{-- <div class="row">
                                                    <div class="col-md-12">
                                                        <input data-url="{{route('getFilterResult',$langSysName)}}"
                                                            type="checkbox" data-prop-id="{{$propriety->id}}"
                                                            class="product-filter-check-prop-val-{{$propriety->id}} product-filter-check-prop-val"
                                                            value="{{$propVal->id}}"
                                                            id="property-{{$propriety->id}}-propval-{{$propVal->id}}">
                                                        {{$propVal->name}}
                                                    </div>
                                                </div> --}}
                                                {{-- @endforeach --}}
                                            @endforeach
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </aside>
                        <aside class="wedget__categories pro--range">
                            <h3 class="wedget__title">Filter by price</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <input data-url="{{route('getFilterResult',$langSysName)}}"
                                        id="product-filter-price-min-val" placeholder="De la" type="number"
                                        class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input data-url="{{route('getFilterResult',$langSysName)}}"
                                        id="product-filter-price-max-val" placeholder="Pina la" type="number"
                                        class="form-control">
                                </div>
                            </div>
                        </aside>

                    </div>
                </div>
                <div class="col-lg-9 col-12 order-1 order-lg-2">

                    <div class="tab__container">
                        <div class="shop-grid tab-pane fade show active" id="nav-grid" role="tabpanel">
                            <div class="row">
                                @foreach ($products as $product)
                                <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product__thumb">
                                        <a class="first__img"
                                            href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
                                            <img style="height:300px"
                                                src="/images/products/{{$product->images[0]->img}}" alt="product image">
                                        </a>
                                    </div>
                                    <div class="product__content content--center content--center">
                                        <h4>
                                            <a href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
                                                {{$product->prodTrans(App::getLocale())['name']}}
                                            </a>
                                        </h4>
                                        <ul class="prize d-flex">
                                            <li>{{$product->price}} lei</li>
                                        </ul>
                                        <div class="action">
                                            <div class="actions_inner">
                                                <ul class="add_to_links">

                                                    <li>
                                                        <a
                                                            href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                    </li>
                                                    <li data-prod-id="{{$product->id}}" class="prod-add-to-wishlist">
                                                        <a href="">
                                                            <i class="fa fa-shopping-bag"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            {{-- paginare --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-4">
            {{ $products->onEachSide(0)->links() }}
        </div>
    </div>
</div>
@endsection




{{-- 


<div class="accordion" id="filter-props-acordeon">
        @foreach ($type->proprieties as $propriety)
        <div class="card">
            <div class="p-2 card-header" id="prop-card-{{$propriety->id}}">
<h2 class="m-0">
    <div class="row">
        <div class="col-md-1">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="property-{{$propriety->id}}">
                <label class="custom-control-label" for="property-{{$propriety->id}}">

                </label>
            </div>
        </div>
        <div class="col-md-10">
            <button
                class="text-left btn-block p-0 btn @if($propriety->val_is_number==0) dropdown-toggle @endif collapsed"
                type="button" data-toggle="collapse" data-target="#prop-card-{{$propriety->id}}-collapse"
                aria-expanded="false" aria-controls="prop-card-{{$propriety->id}}-collapse">
                {{$propriety->name}}
            </button>
        </div>
    </div>
</h2>
</div>
@if($propriety->val_is_number==0)
<div id="prop-card-{{$propriety->id}}-collapse" class="collapse" aria-labelledby="prop-card-{{$propriety->id}}"
    data-parent="#filter-props-acordeon">
    <div class="card-body">
        @foreach ($propriety->propVal as $propVal)
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input"
                id="property-{{$propriety->id}}-propval-{{$propVal->id}}">
            <label class="custom-control-label" for="property-{{$propriety->id}}-propval-{{$propVal->id}}">
                {{$propVal->name}}
            </label>
        </div>
        @endforeach
    </div>
</div>
@endif
</div>
@endforeach
</div> --}}
