@extends('layouts.app')

@section('content')
<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-md-12 p-0">
            <img src="https://picsum.photos/id/583/1800/350" class="img-responsive category-img">
            <div class="mb-5 pb-5 carousel-caption">
                <h1>Categories</h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row mt-3">
        @foreach ($categories as $category)
        <div class="col-md-4">
            <div class="product product__style--3">
                <div class="product__thumb">
                    <a class="first__img" href="{{route('category',['id'=>$category->id,'lang'=>$langSysName])}}">
                        <img style="height:275px;" src="/images/categories/{{$category->img}}" alt="product image">
                    </a>
                </div>
                <div class="product__content content--center">
                    <h4><a href="{{route('category',['id'=>$category->id,'lang'=>$langSysName])}}">
                            {{$category->getCategoryTrans($langSysName)['name']}}
                        </a></h4>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <div class="row mt-3">
        <div class="col-md-4">
            {{ $categories->onEachSide(0)->links() }}
        </div>
    </div>
</div>

@endsection
