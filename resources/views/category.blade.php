@extends('layouts.app')

@section('content')
<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-md-12 p-0">
            <img src="https://picsum.photos/id/583/1800/350" class="img-responsive category-img">
            <div class="mb-5 pb-5 carousel-caption">
                <h1>{{$category->getCategoryTrans($langSysName)['name']}}</h1>
            </div>

        </div>
    </div>
</div>

<div class="container">
    <div class="row mt-3">
        @foreach ($types as $type)
        <div class="col-md-4">
            <a class="link" href="{{route('type',['id'=>$type->id,'lang'=>$langSysName])}}">
                <div class="card bg-transparent border-0">
                    <img style="height:300px;" src="/images/types/{{$type->img}}">
                    <div class="card-body">
                        <h5 class="card-title text-center text-dark">{{$type->getTypeTrans($langSysName)['name']}}</h5>
                        {{-- <p class="card-text">{{str_limit($shop->description, $limit = 30, $end = '...')}}</p> --}}
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>

    <div class="row mt-3">
        <div class="col-md-4">
            {{ $types->onEachSide(0)->links() }}
        </div>
    </div>
</div>

@endsection
