@extends('layouts.app')

@section('content')
<div class="pt-5 container">
    <div class="row my-5 justify-content-center">
        <div class="col-lg-10">
            <h1 class="mt-4">
                {{$post->postTrans($langSysName)['title']}}
            </h1>
            <hr>
            <p>
                Posted on {{date_format($post->created_at, 'd.m.y , g:i a')}}
            </p>
            <hr>
            <img style="width:100%" class="img-fluid rounded" src="/images/cover/{{$post->img}}" alt="">
            <hr>
            {!! $post->postTrans($langSysName)['description']!!}
        </div>
    </div>
</div>
@endsection
