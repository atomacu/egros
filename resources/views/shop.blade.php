@extends('layouts.app')

@section('content')
<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-md-12 p-0">
            <img src="https://picsum.photos/id/583/1800/350" class="img-responsive category-img">
            <div class="mb-5 pb-5 carousel-caption">
                <h1>Shop name</h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
                    <div class="shop__sidebar">
                        <aside class="wedget__categories poroduct--cat">
                            <h3 class="wedget__title">Product Categories</h3>
                            <ul>
                                <li><a href="#">Biography <span>(3)</span></a></li>
                                <li><a href="#">Business <span>(4)</span></a></li>
                                <li><a href="#">Cookbooks <span>(6)</span></a></li>
                                <li><a href="#">Health & Fitness <span>(7)</span></a></li>
                                <li><a href="#">History <span>(8)</span></a></li>
                                <li><a href="#">Mystery <span>(9)</span></a></li>
                                <li><a href="#">Inspiration <span>(13)</span></a></li>
                                <li><a href="#">Romance <span>(20)</span></a></li>
                                <li><a href="#">Fiction/Fantasy <span>(22)</span></a></li>
                                <li><a href="#">Self-Improvement <span>(13)</span></a></li>
                                <li><a href="#">Humor Books <span>(17)</span></a></li>
                                <li><a href="#">Harry Potter <span>(20)</span></a></li>
                                <li><a href="#">Land of Stories <span>(34)</span></a></li>
                                <li><a href="#">Kids' Music <span>(60)</span></a></li>
                                <li><a href="#">Toys & Games <span>(3)</span></a></li>
                                <li><a href="#">hoodies <span>(3)</span></a></li>
                            </ul>
                        </aside>
                        <aside class="wedget__categories pro--range">
                            <h3 class="wedget__title">Filter by price</h3>
                            <div class="content-shopby">
                                <div class="price_filter s-filter clear">
                                    <form action="#" method="GET">
                                        <div id="slider-range"></div>
                                        <div class="slider__range--output">
                                            <div class="price__output--wrap">
                                                <div class="price--output">
                                                    <span>Price :</span><input type="text" id="amount" readonly="">
                                                </div>
                                                <div class="price--filter">
                                                    <a href="#">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </aside>

                    </div>
                </div>
                <div class="col-lg-9 col-12 order-1 order-lg-2">

                    <div class="tab__container">
                        <div class="shop-grid tab-pane fade show active" id="nav-grid" role="tabpanel">
                            <div class="row">
                                @foreach ($products as $product)
                                <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product__thumb">
                                        <a class="first__img" href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
                                            <img style="height:300px" src="/images/products/{{$product->images[0]->img}}" alt="product image">
                                        </a>
                                    </div>
                                    <div class="product__content content--center content--center">
                                        <h4><a
                                                href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">{{$product->prodTrans(App::getLocale())['name']}}</a>
                                        </h4>
                                        <ul class="prize d-flex">
                                            <li>{{$product->price}} lei</li>
                                        </ul>
                                        <div class="action">
                                            <div class="actions_inner">
                                                <ul class="add_to_links">

													<li>
														<a href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
														<i class="fa fa-search"></i>
													</a>
                                                    </li>
													<li data-prod-id="{{$product->id}}" class="prod-add-to-wishlist">
														<a href="">
															<i class="fa fa-shopping-bag"></i>
														</a>
													</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach




                            </div>


                            {{-- paginare --}}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-4">
            {{ $products->onEachSide(0)->links() }}
        </div>
    </div>
</div>

@endsection
