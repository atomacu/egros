@extends('layouts.app')

@section('content')
<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-md-12 p-0">
            <img src="https://picsum.photos/id/583/1800/350" class="img-responsive category-img">
            <div class="mb-5 pb-5 carousel-caption">
                <h1>Blog posts</h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        @foreach ($posts as $post)
        <div class="col-md-6">
            <a class="link" href="{{route('post',['id'=>$post->id,'lang'=>$langSysName])}}">
                <div class="card bg-transparent mt-4">
                    <img src="/images/cover/{{$post->img}}" style="width:100%;" alt="slider 01">
                    <div class="card-body">
                        <h5 class="card-title text-center text-dark">
                            {{$post->postTrans($langSysName)['title']}}
                            <p class="card-text">
                                {!! str_limit(strip_tags($post->postTrans($langSysName)['description']), $limit = 45,
                                $end = '...')!!}
                            </p>
                        </h5>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-4">
        {{ $posts->onEachSide(0)->links() }}
    </div>
</div>
@endsection
