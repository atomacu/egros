@extends('layouts.app')

@section('content')
<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-md-12 p-0">
            <img src="https://picsum.photos/id/583/1800/350" class="img-responsive category-img">
            <div class="mb-5 pb-5 carousel-caption">
                <h1>Shops</h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="furniture--4 border--round arrows_style owl-carousel owl-theme row mt--50">
        @foreach ($shops as $shop)
        <div class="product product__style--3">
            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                <div class="product__thumb">
                    <a class="first__img" href="{{route('shop',['id'=>$shop->id,'lang'=>$langSysName])}}">
                        <img style="height:200px;" src="/images/shops/{{$shop->img}}" alt="product image">
                    </a>
                    <a class="second__img animation1" href="{{route('shop',['id'=>$shop->id,'lang'=>$langSysName])}}">
                        <img style="height:200px;" src="/images/shops/{{$shop->img}}" alt="product image">
                    </a>
                </div>
                <div class="product__content content--center content--center">
                    <h4><a href="{{route('shop',['id'=>$shop->id,'lang'=>$langSysName])}}">{{$shop->name}}</a></h4>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


<div class="row mt-3">
    <div class="col-md-4">
        {{ $shops->onEachSide(0)->links() }}
    </div>
</div>
@endsection
