@foreach ($type->proprieties as $i=>$propriety)
<div class="card border-0">
    <div class="p-2" id="prop-card-{{$propriety->id}}">
        <h2 class="m-0">
            <div class="row">
                <div class="col-md-10">
                    <input data-url="{{route('getFilterResult',$langSysName)}}" type="checkbox"
                        class="product-filter-check-property" value="{{$propriety->id}}"
                        id="property-{{$propriety->id}}">
                    <button class="text-left p-0 btn @if($propriety->val_is_number==0) dropdown-toggle @endif collapsed"
                        type="button" data-toggle="collapse" data-target="#prop-card-{{$propriety->id}}-collapse"
                        aria-expanded="false" aria-controls="prop-card-{{$propriety->id}}-collapse">
                        {{$propriety->name}}
                    </button>
                </div>
            </div>
        </h2>
    </div>
    @if($propriety->val_is_number==0)
    <div id="prop-card-{{$propriety->id}}-collapse" class="collapse" aria-labelledby="prop-card-{{$propriety->id}}"
        data-parent="#filter-props-acordeon">
        <div class="pt-0 card-body">
            @foreach ($propriety->propVal as $propVal)
            <div class="row">
                <div class="col-md-12">
                    <input data-url="{{route('getFilterResult',$langSysName)}}" type="checkbox"
                        class="product-filter-check-prop-val" value="{{$propVal->id}}"
                        id="property-{{$propriety->id}}-propval-{{$propVal->id}}">
                    {{$propVal->name}}
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
</div>
@endforeach
