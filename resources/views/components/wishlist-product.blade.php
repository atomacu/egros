<div>
    <div class="row mx-1">
        <div data-prod-id="{{$product->id}}"
            class="h3 m-0 pt-5 link wishlist-product-table-row-remove-btn text-center text-capitalize p-3 border col-md-2">
            <i class="fa fa-times"></i>
        </div>
        <div class="text-center text-capitalize p-3 border col-md-4">
            <a class="link" href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
                <img style="width:200px;" alt="" src="/images/products/{{$product->images[0]->img}}">
            </a>
        </div>
        <div class="h5 m-0 pt-4 text-center text-capitalize p-3 border col-md-3">
            <a class="link" href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
                {{$product->prodTrans($langSysName)->name}}
            </a>
        </div>
        <div class="h5 m-0 pt-4 text-center text-capitalize p-3 border col-md-3">
            {{$product->price}} lei
        </div>
    </div>
    <hr class="new4">
</div>
