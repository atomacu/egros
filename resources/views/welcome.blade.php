@extends('layouts.app')

@section('content')
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="https://picsum.photos/id/1001/1920/950" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="https://picsum.photos/id/39/1920/950" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="https://picsum.photos/id/43/1920/950" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>




<section class="wn__product__area brown--color pt--80  pb--30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center">
                    <h2 class="title__be--2">Our <span class="color--theme">shops</span></h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                        suffered lebmid alteration in some ledmid form</p>
                </div>
            </div>
        </div>
        <!-- Start Single Tab Content -->
        <div class="furniture--4 border--round arrows_style owl-carousel owl-theme row mt--50">
            @foreach ($shops as $shop)
                <div class="product product__style--3">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                        <div class="product__thumb">
                            <a class="first__img" href="{{route('shop',['id'=>$shop->id,'lang'=>$langSysName])}}">
                                <img style="height:200px;" src="/images/shops/{{$shop->img}}" alt="product image">
                            </a>
                            <a class="second__img animation1" href="{{route('shop',['id'=>$shop->id,'lang'=>$langSysName])}}">
                                <img style="height:200px;" src="/images/shops/{{$shop->img}}" alt="product image">
                            </a>
                        </div>
                        <div class="product__content content--center content--center">
                            <h4><a href="{{route('shop',['id'=>$shop->id,'lang'=>$langSysName])}}">{{$shop->name}}</a></h4>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="row pb-5 mt-3 justify-content-center">
            <div class="col-md-4">
                <a href="{{route('shops',['lang'=>$langSysName])}}" class="d-block link text-center h3">
                    See all shops<br> <i class="fas fa-angle-double-down"></i>
                </a>
            </div>
        </div>
        <!-- End Single Tab Content -->
    </div>
</section>


<section class="wn__newsletter__area bg-image--2">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 offset-lg-5 col-md-12 col-12 ptb--150">

            </div>
        </div>
    </div>
</section>


<section class="wn__bestseller__area bg--white pt--80  pb--30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center">
                    <h2 class="title__be--2">
                        {{-- //all --}}
                        <span class="color--theme">Categories</span>
                    </h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                        suffered lebmid alteration in some ledmid form</p>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach ($categories as $category)
            <div class="col-md-4">
                <div class="product product__style--3">
                    <div class="product__thumb">
                        <a class="first__img" href="{{route('category',['id'=>$category->id,'lang'=>$langSysName])}}">
                            <img style="height:275px;" src="/images/categories/{{$category->img}}" alt="product image">
                        </a>
                    </div>
                    <div class="product__content content--center">
                        <h4><a href="{{route('category',['id'=>$category->id,'lang'=>$langSysName])}}">
                                {{$category->getCategoryTrans($langSysName)['name']}}
                            </a></h4>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="row pb-5 mt-3 justify-content-center">
            <div class="col-md-4">
                <a href="{{route('categories',$langSysName)}}" class="d-block link text-center h3">
                    See all categories<br> <i class="fas fa-angle-double-down"></i>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="wn__recent__post bg--gray ptb--80">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center">
                    <h2 class="title__be--2">Our <span class="color--theme">Blog</span></h2>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                        suffered lebmid alteration in some ledmid form</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt--50">
            @foreach ($posts as $post)
            <div class="col-md-6 col-lg-4 col-sm-12">
                <div class="post__itam">
                    <div class="content">
                        <h3><a href="{{route('post',['id'=>$post->id,'lang'=>$langSysName])}}">{{$post->postTrans($langSysName)['title']}}
                            </a></h3>
                        <p>
                            {!! str_limit(strip_tags($post->postTrans($langSysName)['description']), $limit = 65, $end =
                            '...')!!}
                        </p>
                        <div class="post__time">
                            <span class="day">Dec 06, 18</span>
                            <div class="post-meta">
                                <ul>
                                    <li><a href="#"><i class="bi bi-love"></i>72</a></li>
                                    <li><a href="#"><i class="bi bi-chat-bubble"></i>27</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach



        </div>

    </div>
    <div class="row pb-5 mt-5 justify-content-center">
        <div class="col-md-4">
            <a href="{{route('posts',$langSysName)}}" class="d-block link text-center h3">
                See blogs<br> <i class="fas fa-angle-double-down"></i>
            </a>
        </div>
    </div>
</section>

@endsection
