<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('css/cropper.css') }}">

    <!-- Styles -->
    <link href='https://fonts.googleapis.com/css?family=Ubuntu+Mono' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sbadmin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

</head>

<body>
    <div id="wrapper">

        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route("wellcome",Auth::user()->default_lang)}}">
                <div class="sidebar-brand-text mx-3">Egros</div>
            </a>
            <hr class="sidebar-divider">

            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    Dashboard<i class="fas fa-tachometer-alt ml-1"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin-general-settings.index')}}">
                    General settings<i class="fas fa-tools ml-1"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin-shop.index')}}">
                    Shops managment<i class="fas fa-store ml-1"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin-blog.index')}}">
                    Blog<i class="far fa-newspaper ml-1"></i>
                </a>
            </li>


            <hr class="sidebar-divider d-none d-md-block">


            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <ul class="navbar-nav ml-auto">

                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link " href="#" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link " href="#" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()::getLang()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @foreach($langs as $lang)
                                    @if(Auth::user()->default_lang!=$lang->system_name)
                                        <a data-url="{{route('admin-lang.update',$lang->id)}}" data-sys-name="{{$lang->system_name}}" class="change-lang dropdown-item">
                                            {{$lang->name}}
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </li>
                        @endguest
                    </ul>

                </nav>

                <div class="container-fluid">
                    @yield('content')
                </div>

            </div>

            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; SoftChamp 2019</span>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    
    <script src="{{ asset('js/jquery-3-4-1.js') }}" defer></script>
    <script src="{{ asset('js/cropper.js') }}" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/sbadmin.js') }}" defer></script>
    <script src="{{ asset('js/axios.js') }}" defer></script>
    <script src="{{ asset('js/admin-script.js') }}" defer></script>
    <script src="{{ asset('js/global-script.js') }}" defer></script>

</body>

</html>
