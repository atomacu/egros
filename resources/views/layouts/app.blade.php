<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style2.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
    integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="{{ asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body data-wish-prod-url="{{ route('wishlistProduct', $langSysName) }}">
    <div id="app">
        <header id="wn__header" class="header__area header__absolute sticky__header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-6 col-lg-2">
                        <div class="drop logo">
                                <a class="p-0 navbar-brand" href="{{ route('wellcome',$langSysName) }}">
                                        {{ config('app.name', 'EGROS') }}
                                </a>
                        </div>
                    </div>
                    <div class="col-lg-8 d-none d-lg-block">
                        <nav class="mainmenu__nav">
                            <ul class="meninmenu d-flex justify-content-start">
                                <li class="drop with--one--item"><a href="/home">Home</a></li>
                                <li class="drop">
                                    <a href="{{route('about',$langSysName)}}">About</a>
                                </li>
                                <li class="drop">
                                    <a href="{{route('services',$langSysName)}}">Services</a>
                                </li>
                                <li class="drop">
                                    <a href="{{route('posts',$langSysName)}}">Blog</a>
                                </li>
                                <li class="drop">
                                    <a href="{{route('contact',$langSysName)}}">Contact</a>
                                </li>
                                <li class="drop">
                                    <a href="">Egros Map</a>
                                </li>

                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-6 col-6 col-lg-2">
                        <ul class="header__sidebar__right d-flex justify-content-end align-items-center">
                            <li class="nav-item dropdown">
                                <a class="nav-link " href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    {{$langs[0]::getLangBySysName(App::getLocale())['name']}}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    @foreach ($langs as $lang)
                                    @if ($lang::getLangBySysName(App::getLocale())['id']!=$lang->id)
                                    <a class="dropdown-item"
                                        href="{{ route( Route::currentRouteName(),$lang->system_name ) }}">{{$lang->name}}</a>
                                    @endif
                                    @endforeach
                                </div>
                            </li>
                            <li class="shopcart">
                                <a href="{{route('wishlist',$langSysName)}}">
                                    <span id="app-wishlist-count" class="product_qun"></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
               
                <div class="row d-none">
                    <div class="col-lg-12 d-none">
                        <nav class="mobilemenu__nav">
                            <ul class="meninmenu">
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Pages</a>
                                </li>
                           
                                <li>
                                    <a href="contact.html">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
              
                <div class="mobile-menu d-block d-lg-none">
                </div>
               
            </div>
        </header>
    </div>
    <main class="min-vh-100">
        @yield('content')
    </main>
    <footer id="wn__footer" class="footer__area bg__cat--8 brown--color">
        <div class="footer-static-top pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__widget footer__menu">
                            <div class="ft__logo">
                                <a class="display-4" href="index.html">
                                    EGROS
                                </a>
                            </div>
                            <div class="footer__content">
                                <ul class="social__net social__net--2 d-flex justify-content-center">
                                    <li><a href="#"><i class="bi bi-facebook"></i></a></li>
                                    <li><a href="#"><i class="bi bi-google"></i></a></li>
                                    <li><a href="#"><i class="bi bi-twitter"></i></a></li>
                                    <li><a href="#"><i class="bi bi-linkedin"></i></a></li>
                                    <li><a href="#"><i class="bi bi-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright__wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="copyright">
                            <div class="copy__right__inner text-left">
                                <p>Copyright 
                                    <i class="fa fa-copyright"></i>
                                    <a href="">
                                        SoftChamp
                                    </a> 
                                    All Rights Reserved
                                </p>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </footer>
    </div>
    <script src="{{ asset('js/jquery-3-4-1.js') }}" defer></script>
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="{{ asset('js/axios.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>
    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{ asset('js/active.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>

</html>
