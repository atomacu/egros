@extends('layouts.app')

@section('content')
<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-md-12 p-0">
            <img src="https://picsum.photos/id/583/1800/350" class="img-responsive category-img">
            <div class="mb-5 pb-5 carousel-caption">
                <h1>{{$product->type->getTypeTrans($langSysName)['name']}}</h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-md-5 product_content">
            <h3 class="text-dark"></h3>

            <div class="space-ten"></div>
            <h4 class="cost">

            </h4>


            <div class="row">
                <div class="col-md-6 text-center">

                </div>
            </div>
        </div>
    </div>


    <div class="mt-5 justify-content-center row">
        <div class="col-lg-9 col-12">
            <div class="wn__single__product">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <div class="wn__fotorama__wrapper">
                            <div class="fotorama wn__fotorama__action" data-nav="thumbs">
                                @foreach ($product->images as $i=>$img)
                                <a href="/images/products/{{$img->img}}"><img src="/images/products/{{$img->img}}"
                                        alt=""></a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="product__info__main">
                            <h1>{{$product->prodTrans(App::getlocale())['name']}}</h1>
                            <h3>Nr. articol: {{$product->id}}</h3>

                            <h4 class="mt-4 cost">
                                <span id="product-cost">
                                    {{$product->price}}
                                </span>
                                <span>
                                    lei
                                </span>
                            </h4>

                        </div>

                        @foreach ($product->productProp as $i=>$productProp)
                        {{-- @if($i!=0)
                        <hr> @endif --}}
                        <div class="row my-2">
                            <div class="col-md-3">
                                {{$productProp->propriety->name}}
                            </div>
                        </div>


                        <div class="row">
                            @foreach ($productProp->prodPropValues as $i1=>$prodPropValue)
                            <div class="col-md-4 text-center">
                                <button data-val1="{{$product->price}}" data-val="{{$prodPropValue->priceDiff}}"
                                    class="prod-property-val btn btn-block btn-primary">
                                    @if($productProp->propriety->val_is_number)
                                    {{$prodPropValue->value}}
                                    @else
                                    {{$prodPropValue->propVal->name}}
                                    @endif
                                </button>
                            </div>
                            @endforeach
                        </div>
                        @endforeach
                        <button data-prod-id="{{$product->id}}" class="mt-4 prod-add-to-wishlist btn btn-success">
                            Add to wishlist<i class="fas fa-heart ml-2"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="product__info__detailed">
                <div class="pro_details_nav nav justify-content-start" role="tablist">
                    <a class="nav-item nav-link active" data-toggle="tab" href="#nav-details" role="tab">Details</a>

                </div>
                <div class="tab__container">
                    <!-- Start Single Tab Content -->
                    <div class="pro__tab_label tab-pane fade show active" id="nav-details" role="tabpanel">
                        <div class="description__attribute">
                            {!! $product->prodTrans(App::getlocale())['description'] !!}
                        </div>
                    </div>

                </div>
            </div>


























            <div class="wn__related__product pt--80 pb--50">
                <div class="section__title text-center">
                    <h2 class="title__be--2">Related Products</h2>
                </div>
                <div class="row mt--60">
                    <div class="productcategory__slide--2 arrows_style owl-carousel owl-theme">
                        <!-- Start Single Product -->

                        @foreach ($products as $product)
                        <div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="product__thumb">
                                <a class="first__img"
                                    href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
                                    <img style="height:300px" src="/images/products/{{$product->images[0]->img}}"
                                        alt="product image">
                                </a>
                            </div>
                            <div class="product__content content--center content--center">
                                <h4><a
                                        href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">{{$product->prodTrans(App::getLocale())['name']}}</a>
                                </h4>
                                <ul class="prize d-flex">
                                    <li>{{$product->price}} lei</li>
                                </ul>
                                <div class="action">
                                    <div class="actions_inner">
                                        <ul class="add_to_links">
                                            <li>
                                                <a
                                                    href="{{route('product',['id'=>$product->id,'lang'=>$langSysName])}}">
                                                    <i class="fa fa-search"></i>
                                                </a>
                                            </li>
                                            <li data-prod-id="{{$product->id}}" class="prod-add-to-wishlist">
                                                <a href="">
                                                    <i class="fa fa-shopping-bag"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
