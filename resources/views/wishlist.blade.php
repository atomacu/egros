@extends('layouts.app')

@section('content')
<div class="container-fluid p-0">
    <div class="row p-0 m-0">
        <div class="col-md-12 p-0">
            <img src="https://picsum.photos/id/583/1800/350" class="img-responsive category-img">
            <div class="mb-5 pb-5 carousel-caption">
                <h1>WISHLIST</h1>
            </div>
        </div>
    </div>
</div>

<div id="tbody-wishlist-table" class="d-none container my-5">
    <div class="row mx-1">
        <div class="text-center text-capitalize p-3 bg-dark text-white border col-md-2"><b>remove</b></div>
        <div class="text-center text-capitalize p-3 bg-dark text-white border col-md-4">images</div>
        <div class="text-center text-capitalize p-3 bg-dark text-white border col-md-3">Product</div>
        <div class="text-center text-capitalize p-3 bg-dark text-white border col-md-3">Unit Price</div>
    </div>
    <hr class="new4">
</div>

<div id="wishlist-empty-container" class="container">
    <div class="row mt-5">
        <div class="col-md-12 text-center">
            <h1 class="display-4">Your wishlist is empty</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <img style="width:55%; display: block; margin-left: auto; margin-right: auto; width: 50%;"
                src="/images/emptycart.png" alt="">
        </div>
    </div>
</div>
@endsection
