@extends('layouts.'.Auth::user()->userRole->role->default_page)

@section('content')
<div class="row">
    <div class="col-md-5 product_img">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                @foreach ($product->images as $i=>$img)
                <div class="carousel-item  @if($i==0) active @endif">
                    <img src="/images/products/{{$img->img}}" class="img-responsive d-block w-100">
                </div>
                @endforeach

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="col-md-7 product_content">
        <h3 class="text-dark">{{$product->prodTrans(Auth::user()::getLang()->system_name)['name']}}</h3>

        {!! $product->prodTrans(Auth::user()::getLang()->system_name)['description'] !!}

        @foreach ($product->productProp as $i=>$productProp)
        @if($i!=0)
        <hr> @endif
        <div class="row my-2">
            <div class="col-md-3">
                {{$productProp->propriety->name}}
            </div>
            <div class="col-md-3">
                <div class="row">
                    @foreach ($productProp->prodPropValues as $i1=>$prodPropValue)
                    <div class="col-md-12 text-center">
                        @if($i1!=0)
                        <hr> @endif
                        @if($productProp->propriety->val_is_number)
                        {{$prodPropValue->value}}
                        @else
                        {{$prodPropValue->propVal->name}}
                        @endif
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    @foreach ($productProp->prodPropValues as $i1=>$prodPropValue)
                        <div class="col-md-12 text-center">
                            @if($i1!=0)
                                <hr> 
                            @endif
                            {{$prodPropValue->priceDiff}} lei
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
        @endforeach
        <div class="space-ten"></div>
        <h4 class="cost"> {{$product->price}} lei</h4>
        <div class="btn-ground">
            <ul class="social">
                <li class="d-block"><a href="" class="d-block w-100" data-tip="Add to Wishlist"><i
                            class="fa fa-shopping-bag"></i></a></li>
            </ul>
        </div>
    </div>
</div>
@endsection
