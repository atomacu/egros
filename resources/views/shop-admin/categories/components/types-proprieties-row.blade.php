<div id="types-proprieties-row" class="row justify-content-center">
    <div class="col-md-12">
        @foreach ($proprieties as $i=>$propriety)

            <div class="row justify-content-center">
                <div class="col-md-10">
                    <hr>
                </div>
            </div>
            <div class="row mt-3 justify-content-center">
                <div class="col-md-1">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox"  data-is-number="{{$propriety->val_is_number}}" data-prop-id="{{$propriety->id}}" class="type-property-is-checked" aria-label="Checkbox for following text input">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    {{$propriety->name}}
                </div>
                <div class="col-md-4">
                    @if ($propriety->val_is_number==1)
                        <input type="text" placeholder="Inser value" class="type-property-val-{{$propriety->id}} form-control">
                    @elseif($propriety->val_is_number==0)
                        <select class="form-control type-property-val-{{$propriety->id}}">
                            <option value="">Select value</option>
                            @foreach ($propriety->propVal as $value)
                            <option value="{{$value->id}}">{{$value->name}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
                <div class="col-md-3">
                    <input type="text" placeholder="Price difference" class="type-property-val-input-price-diff-{{$propriety->id}} form-control">
                </div>
            </div>
            <div id="add-propery-val-row-{{$propriety->id}}" class="row mt-3 justify-content-center">
                <div class="col-md-3"></div>
                <div class="col-md-7">
                    <button type="button" data-url="{{route('shop_admin_property.show',$propriety->id)}}" data-property-id="{{$propriety->id}}" class="add-propery-val-btn btn btn-primary btn-block"><i class="fas fa-plus"></i></button>
                </div>
            </div>
            @if (count($proprieties)-1==$i)
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <hr>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
