<div id="property-new-val-row-{{$propriety->id}}" class="row mt-2 justify-content-center">
    <div class="col-md-3 text-center">
        <i data-property-id="{{$propriety->id}}" class="link fas fa-times text-danger delete-property-new-val-row"></i>
    </div>
    <div class="col-md-4">
        @if ($propriety->val_is_number==1)
        <input type="text" placeholder="Inser value" class="type-property-val-{{$propriety->id}} form-control">
        @elseif($propriety->val_is_number==0)
        <select class="form-control type-property-val-{{$propriety->id}}" name="" id="">
            <option value="">Select value</option>
            @foreach ($propriety->propVal as $value)
            <option value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
        </select>
        @endif
    </div>
    <div class="col-md-3">
        <input type="text" placeholder="Price difference" class="type-property-val-input-price-diff-{{$propriety->id}} form-control">
    </div>
</div>
