@extends('layouts.'.Auth::user()->userRole->role->default_page)

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <img style="width:100%;" src="/images/categories/{{$categ->img}}" alt="">
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h2>{{$categ->name}}</h2>
                    </div>
                </div>

                @foreach ($langs as $lang)
                <div class="row">
                    <div class="col-md-2">
                        <span class="monospaced">{{$lang->name}}</span>
                    </div>
                    <div class="col-md-10">
                        <p>
                            {{$categ->getCategoryTrans($lang->id)['name']}}
                        </p>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#add-product-modal">
                    <i class="fas fa-plus"></i>
                </button>

                <div class="modal fade" id="add-product-modal" tabindex="-1" role="dialog"
                    aria-labelledby="add-product-modal-label" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="add-product-modal-label">Add product</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form data-shop-id="{{$shopMember->shop->id}}"
                                    data-url="{{route("shop_admin_product.store")}}" id="add-product-form">
                                    @foreach ($langs as $i=>$lang)
                                    <div data-lang-id="{{$lang->id}}"
                                        class="add-product-product-name-and-descript-group row mb-2 justify-content-center">
                                        <div class="col-md-10">
                                            <input type="text" placeholder="Product name in {{$lang->name}}"
                                                id="add-product-product-name-{{$lang->id}}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mb-2 justify-content-center">
                                        <div class="col-md-10">
                                            <textarea class="add-product-product-description-{{$lang->id}} form-control"
                                                placeholder="Product description in {{$lang->name}}"
                                                id="editor-{{$i}}"></textarea>
                                        </div>
                                    </div>
                                    @endforeach
                                    <hr>
                                    <div class="row mb-2 justify-content-center">
                                        <div class="col-md-10">
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="images[]"
                                                        id="input-product-images"
                                                        aria-describedby="input-product-images-addon-1" multiple>
                                                    <label class="custom-file-label" for="input-product-images">Choose
                                                        images</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mb-2 justify-content-center">
                                        <div id="image-input-preview-galery" class="col-md-10"></div>
                                    </div>
                                    <hr>
                                    <div id="product-types-row" class="row mb-2 justify-content-center">
                                        <div class="col-md-10">
                                            <select class="form-control" id="add-product-select-type">
                                                <option value="" selected disabled>Select type</option>
                                                @foreach ($categ->types as $type)
                                                <option data-url="{{route('shop_admin_type.show',$type->id)}}"
                                                    value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="row mb-2 justify-content-center">
                                        <div class="col-md-10">
                                            <input type="text" id="add-product-product-base-price" placeholder="Price"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-md-10">
                                            <button class="btn btn-success btn-block">Add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            @foreach ($products as $product)
            <div class="mt-4 col-lg-4">
                <div class="product-grid6">
                    <div class="product-image6">
                        <a href="#">
                            <img class="pic-1" src="/images/products/{{$product->images[0]->img}}">
                        </a>
                    </div>
                    <div class="product-content">
                        <h3 class="title"><a href="#">{{$product->prodTrans(Auth::user()::getLang()->system_name)['name']}}</a></h3>
                        <div class="price">{{$product->price}} lei</div>
                    </div>
                    <ul class="social">
                        <li><a href="{{route('shop_admin_product.show',$product->id)}}"><i class="fa fa-search"></i></a></li>
                        <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-success btn-block">
                            <i class="fas fa-pencil-alt mr-2"></i>Edit
                        </button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-danger btn-block" data-toggle="modal" data-target="#delete-product-modal">
                            <i class="fas fa-trash-alt mr-2"></i>Delete
                        </button>

                        <div class="modal fade" id="delete-product-modal" tabindex="-1" role="dialog"
                            aria-labelledby="delete-product-modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="delete-product-modalLabel">Are you sure that you want to delete this product?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="row justify-content-center">
                                            <div class="col-md-5">
                                                <button type="button" class="btn btn-success btn-block" data-dismiss="modal">No</button>
                                            </div>
                                           <div class="col-md-5">
                                               <button data-url="{{route('shop_admin_product.destroy',$product->id)}}" class="delete-product-confirm-btn btn btn-danger btn-block">Yes</button>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach
        </div>

    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-dark text-center">Types</h3>
            </div>
        </div>
        <hr>
        <div class="ml-3 row">
            @foreach ($categ->types as $type)
            <div class="col-md-12">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        {{$type->name}}
                    </label>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
